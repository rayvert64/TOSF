package Ipc::Packet::Tnet;
#================================================================--
# File Name    : Packet/Tnet.pm
#
# Purpose      : implements Tnet packet ADT
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

use constant PCHAR => '_T_';

sub  new {
   my $class = shift @_;
   my $name = shift @_;

   my $self = {
      type => my $type,
      msg => my $msg
   };

   bless ($self, $class);

   return $self;
}

sub get_type {
   my $self = shift @_;

   return $self->{type};
}

sub set_type {
   my $self = shift @_;
   my $t = shift @_;
 
   if (defined($t)) {
      $self->{type} = $t;
   }

   return;
}

sub get_msg {
   my $self = shift @_;

   return $self->{msg};
}

sub set_msg {
   my $self = shift @_;
   my $ms = shift @_;

   if (defined($ms)) {
      $self->{msg} = $ms;
   }

   return;
}

sub encode {
   my $self = shift @_;

   my @m;
   $m[0] = $self->{type};
   $m[1] = $self->{msg};

   return join(PCHAR, @m);
}

sub decode {
   my $self = shift @_;
   my $pkt = shift @_;

   my @m = split(PCHAR, $pkt);

   $self->{type} = $m[0];
   $self->{msg} = $m[1];

   return;
}

sub dump {
   my $self = shift @_;

   print ("TYPE: $self->{type} \n");
   print ("MSG: $self->{msg} \n");

   return;
}

1;
