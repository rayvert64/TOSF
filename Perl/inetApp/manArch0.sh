#!/bin/sh
#========================================================
# Project      : Time Oriented Software Framework
#
# File Name    : manArch0.sh
#
# Purpose      : Deploy MAN Architecture 0
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Dash Shell Script (Linux)
#
#========================================================

printf "0\n1\n5070\nnone\n" > .k1 
xterm -fa 'Monospace' -fs 12 -geometry 68x20+0+300 -title "Hub0" -hold -e "./main.pl .k1" &
printf "0\n1\n6070\nnone\n" > .k2 
xterm -fa 'Monospace' -fs 12 -geometry 68x20+0+400 -title "Hub1" -hold -e "./main.pl .k2" &
sleep 3
printf "1\n1\n0\n" > .k3 
#xterm -fa 'Monospace' -fs 12 -geometry 68x20+0+600 -title "Peter" -hold -e "./main.pl .k3" &
printf "1\n1\n1\n" > .k4 
xterm -fa 'Monospace' -fs 12 -geometry 70x20+420+600 -title "Paul" -hold -e "./main.pl .k4" &
printf "1\n1\n2\n" > .k5
xterm -fa 'Monospace' -fs 12 -geometry 70x20+900+300 -title "Mary" -hold -e "./main.pl .k5" &
printf "1\n1\n3\n" > .k6
xterm -fa 'Monospace' -fs 12 -geometry 70x20+800+300 -title "Router" -hold -e "./main.pl .k6" &
printf "0\n0\nlocalhost\n5071\n" > .k7 
xterm -fa 'Monospace' -fs 12 -geometry 70x20+900+600 -title "Sniffer0" -hold -e "./main.pl .k7" &
printf "0\n0\nlocalhost\n6073\n" > .k8 
xterm -fa 'Monospace' -fs 12 -geometry 64x20+900+600 -title "Sniffer1" -hold -e "./main.pl .k8" &

#sleep 3
#ssh -Y -l pwalsh otter.csci.viu.ca 'cd /home/faculty/pwalsh/XOX/HACK/EXAM/TosfDev/Perl/inetApp/ ; printf "0\n1\n5070\nnone\n" > .kk ; nohup xterm -ls -fa 'Monospace' -fs 12 -geometry 68x20+0+300 -title "Hub" -hold -e "./main.pl .kk" &  ' 
#
