package inetApp::Packet::Tftp;
#================================================================--
# File Name    : Packet/Tftp.pm
#
# Purpose      : implements Tftp packet ADT
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

use constant PCHAR => '_R_';

my $type = ' ';
my $seq_num = ' ';
my $block_num = ' ';
my $msg = ' ';

sub  new {
   my $class = shift @_;
   my $name = shift @_;

   my $self = {
      type => $type,
      seq_num => $seq_num,
      block_num => $block_num,
      msg => $msg
   };

   bless ($self, $class);

   return $self;
}

sub get_type {
   my $self = shift @_;
   
   return $self->{type};
}

sub set_type {
   my $self = shift @_;
   my $t = shift @_;
 
   if (defined($t)) {
      $self->{type} = $t;
   }

   return;
}

sub get_seq_num {
   my $self = shift @_;

   return $self->{seq_num};
}

sub set_seq_num {
   my $self = shift @_;
   my $s = shift @_;
 
   if (defined($s)) {
      $self->{seq_num} = $s;
   }

   return;
}

sub get_block_num {
   my $self = shift @_;

   return $self->{block_num};
}

sub set_block_num {
   my $self = shift @_;
   my $b = shift @_;
 
   if (defined($b)) {
      $self->{block_num} = $b;
   }

   return;
}

sub get_msg {
   my $self = shift @_;
   my $ms;
   
   $ms = $self->{msg};
   $ms =~ s/_NL_/\n/g;
   return $ms;
}

sub set_msg {
   my $self = shift @_;
   my $ms = shift @_;

   if (defined($ms)) {
      $ms =~ s/\n/_NL_/g;
      $self->{msg} = $ms;
   }

   return;
}

sub encode {
   my $self = shift @_;

   my @m;
   $m[0] = $self->{type};
   $m[1] = $self->{seq_num};
   $m[2] = $self->{block_num};
   $m[3] = $self->{msg};

   return join(PCHAR, @m);
}

sub decode {
   my $self = shift @_;
   my $pkt = shift @_;

   my @m = split(PCHAR, $pkt);

   $self->{type} = $m[0];
   $self->{seq_num} = $m[1];
   $self->{block_num} = $m[2];
   $self->{msg} = $m[3];

   return;
}

sub dump {
   my $self = shift @_;

   print ("Type: $self->{type} \n");
   print ("Sequence Num: $self->{seq_num} \n");
   print ("Block Num: $self->{block_num} \n");
   print ("MSG: $self->{msg} \n");

   return;
}

1;
