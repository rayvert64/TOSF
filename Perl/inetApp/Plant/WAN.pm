package inetApp::Plant::WAN;
#================================================================--
# File Name    : WAN.pm
#
# Purpose      : Plant set-up for WAN 
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;
use constant TRUE => 1;
use constant FALSE => 0;
use constant MAXSEL => 5 ;
use constant IFACEPOLLFREQ => 0.05;
use constant TOMMAC => '2601';
use constant DICKMAC => '2602';
use constant HARRYMAC => '2603';
use constant R1MAC => '2604';
use constant R2MAC => '2605';
use constant R3MAC => '2606';

sub start {
   system('clear');

   my $inp;
   my $sel = -1;
   my $host;
   my $port;

   while (($sel < 0) || ($sel > MAXSEL))  {

      print ("\nWAN Boot Menu\n\n");
      print ("\tTom 0\n");
      print ("\tDick 1\n");
      print ("\tHarry 2\n");
      print ("\tR1 3\n");
      print ("\tR2 4\n");
      print ("\tR3 5\n");
      print ("\nEnter selection (CTRL C to exit) ");
      $inp = <>;
      chop($inp);
      if (($inp =~ m/\d/) && (length($inp) == 1)) {
         $sel = int($inp);
      }

   }

   my $name;

   if ($sel == 0) {

      # Tom 
      Inet::Collection::SYSTEM->set_name("Tom");
      Inet::Collection::SYSTEM->set_type('HOST');

      $host = 'localhost';
      $port = 5070;

      $name = "eth0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      # note, Ethernet, Ip , Icmp etc are set up in Inet::Plant
   
      Inet::Table::ARP->add(
         ip => '192.168.6.1',
          mac => TOMMAC, 
          permanentFlag => 1
      );

      Inet::Table::IFACE->add(
         interface => $name,
         ip => '192.168.6.1',
         mac => TOMMAC,
         packetType => 'ETHERNET' 
      );

      Inet::Table::ROUTE->add(
         network => '192.168.6.1',
         gateway => '0.0.0.0', 
         interface => 'lo'
      );
      Inet::Table::ROUTE->add(
         network => '192.168.6',
         gateway => '0.0.0.0', 
         interface => $name
      );
      Inet::Table::ROUTE->add(
         network => '0.0.0.0', 
         gateway => '192.168.6.2', 
         interface => $name
     );

   } elsif ($sel == 1) {

      # Dick 
      Inet::Collection::SYSTEM->set_name("Dick");
      Inet::Collection::SYSTEM->set_type('HOST');

      $host = 'localhost';
      $port = 5173;

      $name = "eth0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      # note, Ethernet, IP , ICMP are set up in Inet::Plant
   
      Inet::Table::ARP->add(
         ip => '192.168.7.1', 
         mac => DICKMAC, 
         permanentFlag => 1
      );

      Inet::Table::IFACE->add(
         interface => $name,
         ip => '192.168.7.1',
         mac => DICKMAC,
         packetType => 'ETHERNET' 
      );

      Inet::Table::ROUTE->add(
         network => '192.168.7.1', 
         gateway => '0.0.0.0', 
         interface => 'lo'
      );
      Inet::Table::ROUTE->add(
         network => '192.168.7', 
         gateway => '0.0.0.0', 
         interface => $name
      );
      Inet::Table::ROUTE->add(
         network => '0.0.0.0', 
         gateway => '192.168.7.2', 
         interface => $name
      );

   } elsif ($sel == 2) {
      
      # Harry
      Inet::Collection::SYSTEM->set_name("Harry");
      Inet::Collection::SYSTEM->set_type('HOST');

      $host = 'localhost';
      $port = 5273;

      $name = "eth0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      # note, Ethernet, IP , ICMP are set up in Inet::Plant
   
      Inet::Table::ARP->add(
         ip => '192.168.8.1', 
         mac => HARRYMAC, 
         permanentFlag => 1
      );

      Inet::Table::IFACE->add(
         interface => $name,
         ip => '192.168.8.1',
         mac => HARRYMAC,
         packetType => 'ETHERNET' 
      );

      Inet::Table::ROUTE->add(
         network => '192.168.8.1', 
         gateway => '0.0.0.0', 
         interface => 'lo'
      );
      Inet::Table::ROUTE->add(
         network => '192.168.8', 
         gateway => '0.0.0.0', 
         interface => $name
      );
      Inet::Table::ROUTE->add(
         network => '0.0.0.0', 
         gateway => '192.168.8.2', 
         interface => $name
      );

   } elsif ($sel == 3) {

      # R1 
      Inet::Collection::SYSTEM->set_name("R1");
      Inet::Collection::SYSTEM->set_type('HOST');
      Inet::Collection::SYSTEM->set_routerFlag(TRUE);

      $host = 'localhost';
      $port = 5073;

      $name = "eth0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);
   
      Inet::Table::ARP->add(
         ip => '192.168.6.2', 
         mac => R1MAC, 
         permanentFlag => 1
      );

      Inet::Table::IFACE->add(
         interface => $name,
         ip => '192.168.6.2',
         mac => R1MAC,
         packetType => 'ETHERNET' 
      );

      Inet::Table::ROUTE->add(
         network => '192.168.6.2', 
         gateway => '0.0.0.0', 
         interface => 'lo'
      );
      Inet::Table::ROUTE->add(
         network => '192.168.6', 
         gateway => '0.0.0.0', 
         interface => $name
      );

      $port = 6070;

      $name = "p2p0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      Inet::Table::IFACE->add(
         interface => $name,
         ip => '10.0.6.100',
         packetType => 'IP' 
      );

      Inet::Table::ROUTE->add(
         network => '10.0.6.100', 
         gateway => '0.0.0.0', 
         interface => 'lo'
      );
      Inet::Table::ROUTE->add(
         network => '0.0.0.0', 
         gateway => '10.0.6.200', 
         interface => $name
      );

      $port = 6090;

      $name = "p2p1";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      Inet::Table::IFACE->add(
         interface => $name,
         ip => '10.0.8.200',
         packetType => 'IP' 
      );

      Inet::Table::ROUTE->add(
         network => '10.0.8.200', 
         gateway => '0.0.0.0', 
         interface => 'lo'
      );

   } elsif ($sel == 4) {

      # R2 
      Inet::Collection::SYSTEM->set_name("R2");
      Inet::Collection::SYSTEM->set_type('HOST');
      Inet::Collection::SYSTEM->set_routerFlag(TRUE);

      $host = 'localhost';
      $port = 5170;

      $name = "eth0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);
   
      Inet::Table::ARP->add(
         ip => '192.168.7.2', 
         mac => R2MAC, 
         permanentFlag => 1
      );

      Inet::Table::IFACE->add(
         interface => $name,
         ip => '192.168.7.2',
         mac => R2MAC,
         packetType => 'ETHERNET' 
      );

      Inet::Table::ROUTE->add(
         network => '192.168.7.2', 
         gateway => '0.0.0.0', 
         interface => 'lo'
      );
      Inet::Table::ROUTE->add(
         network => '192.168.7', 
         gateway => '0.0.0.0', 
         interface => $name
      );

      $port = 6080;

      $name = "p2p0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      Inet::Table::IFACE->add(
         interface => $name,
         ip => '10.0.7.100',
         packetType => 'IP' 
      );

      Inet::Table::ROUTE->add(
         network => '10.0.7.100', 
         gateway => '0.0.0.0', 
         interface => 'lo'
      );
      Inet::Table::ROUTE->add(
         network => '0.0.0.0', 
         gateway => '10.0.7.200', 
         interface => $name
      );

      $port = 6071;

      $name = "p2p1";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      Inet::Table::IFACE->add(
         interface => $name,
         ip => '10.0.6.200',
         packetType => 'IP' 
      );

      Inet::Table::ROUTE->add(
         network => '10.0.6.200', 
         gateway => '0.0.0.0', 
         interface => 'lo'
      );

   } elsif ($sel == 5) {

      # R3 
      Inet::Collection::SYSTEM->set_name("R3");
      Inet::Collection::SYSTEM->set_type('HOST');
      Inet::Collection::SYSTEM->set_routerFlag(TRUE);

      $host = 'localhost';
      $port = 5270;

      $name = "eth0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);
   
      Inet::Table::ARP->add(
         ip => '192.168.8.2', 
         mac => R3MAC, 
         permanentFlag => 1
      );

      Inet::Table::IFACE->add(
         interface => $name,
         ip => '192.168.8.2',
         mac => R3MAC,
         packetType => 'ETHERNET' 
      );

      Inet::Table::ROUTE->add(
         network => '192.168.8.2', 
         gateway => '0.0.0.0', 
         interface => 'lo'
      );
      Inet::Table::ROUTE->add(
         network => '192.168.8', 
         gateway => '0.0.0.0', 
         interface => $name
      );

      $port = 6091;

      $name = "p2p0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      Inet::Table::IFACE->add(
         interface => $name,
         ip => '10.0.8.100',
         packetType => 'IP' 
      );

      Inet::Table::ROUTE->add(
         network => '10.0.8.100', 
         gateway => '0.0.0.0', 
         interface => 'lo'
      );
      Inet::Table::ROUTE->add(
         network => '0.0.0.0', 
         gateway => '10.0.8.200', 
         interface => $name
      );

      $port = 6081;

      $name = "p2p1";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      Inet::Table::IFACE->add(
         interface => $name,
         ip => '10.0.7.200',
         packetType => 'IP' 
      );

      Inet::Table::ROUTE->add(
         network => '10.0.7.200', 
         gateway => '0.0.0.0', 
         interface => 'lo'
      );

   }

   # All hosts require the following tasks.

   # ========== IFACE CONTROLLER  ============

      $name = "IfaceCon";
      Tosf::Table::TASK->new(
         name => $name,
         periodic => FALSE,
         fsm => Inet::Fsm::IfaceCon->new(
            taskName => $name,
         )
      );
      Tosf::Table::TASK->reset($name);

   # ============== STREAM CONTROLLER  ==============

   $name = "s0";
   Inet::Table::IFACE->set_packetType($name, 'STREAM');
   Tosf::Table::TASK->new(
      name => $name,
      periodic => TRUE,
      period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
      fsm => Inet::Fsm::StreamCon->new(
         taskName => $name,
         ifaceName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   # ============== SHELL  ==============

   $name = "Shell";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => inetApp::Fsm::Shell->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   # ============== APPLICATIONS  ==============

   $name = "Inc";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => inetApp::Fsm::Inc->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   $name = "Incd";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => inetApp::Fsm::Incd->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   $name = "Render";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => inetApp::Fsm::Render->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   $name = "Renderd";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => inetApp::Fsm::Renderd->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   # ================ HEARTBEAT =================

   $name = "HBeat";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => TRUE,
      period => Tosf::Executive::TIMER->s2t(20),
      fsm => Inet::Fsm::HBeat->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);


   # ================ Arp Table Management =================

   $name = "ArpTablePurge";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => TRUE,
      period => Tosf::Executive::TIMER->s2t(1),
      fsm => Inet::Fsm::ArpTablePurge->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

}

1;
