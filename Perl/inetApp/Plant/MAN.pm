package inetApp::Plant::MAN;
#================================================================--
# File Name    : MAN.pm
#
# Purpose      : Plant set-up for MAN 
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;
use constant TRUE => 1;
use constant FALSE => 0;
use constant MAXSEL => 3 ;
use constant IFACEPOLLFREQ => 0.05;
use constant PETERMAC => '2601';
use constant PAULMAC => '2602';
use constant MARYMAC => '2603';
use constant RMAC0 => '2604';
use constant RMAC1 => '2605';

sub start {
   system('clear');

   my $inp;
   my $sel = -1;
   my $port;
   my $host;

   while (($sel < 0) || ($sel > MAXSEL))  {

      print ("\nMAN Boot Menu\n\n");
      print ("\tPeter 0\n");
      print ("\tPaul 1\n");
      print ("\tMary 2\n");
      print ("\tRouter 3\n");
      print ("\nEnter selection (CTRL C to exit) ");
      $inp = <>;
      chop($inp);
      if (($inp =~ m/\d/) && (length($inp) == 1)) {
         $sel = int($inp);
      }

   }

   my $name;

   if ($sel == 0) {

      # Peter 
      Inet::Collection::SYSTEM->set_name("Peter");
      Inet::Collection::SYSTEM->set_type('HOST');

      $host = 'localhost';
      $port = 5070;

      # no parameter checking is performed on this user data :(

      # ================ PORT =================

      $name = "eth0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      # note, Ethernet, Ip , Icmp etc are set up in Inet::Plant
   
      Inet::Table::ARP->add(
         ip => '192.168.6.1',
          mac => PETERMAC, 
          permanentFlag => 1
      );

      #Inet::Table::IFACE->set_ip($name, '192.168.6.1');
      #Inet::Table::IFACE->set_mac($name, PETERMAC);
      #Inet::Table::IFACE->set_packet_type($name, 'ETHERNET');
      Inet::Table::IFACE->add(
         interface => $name,
         ip => '192.168.6.1',
         mac => PETERMAC,
         packetType => 'ETHERNET' 
      );
     

      Inet::Table::ROUTE->add(
         network => '192.168.6.1',
         gateway => '0.0.0.0', 
         interface => 'lo'
      );
      Inet::Table::ROUTE->add(
         network => '192.168.6',
         gateway => '0.0.0.0', 
         interface => $name
      );
      Inet::Table::ROUTE->add(
         network => '0.0.0.0', 
         gateway => '192.168.6.100', 
         interface => $name
     );

   } elsif ($sel == 1) {

      # Paul 
      Inet::Collection::SYSTEM->set_name("Paul");
      Inet::Collection::SYSTEM->set_type('HOST');

      $host = 'localhost';
      $port = 6071;

      # no parameter checking is performed on this user data :(

      # ================ PORT =================

      $name = "eth0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      # note, Ethernet, IP , ICMP are set up in Inet::Plant
   
      Inet::Table::ARP->add(
         ip => '192.168.7.1', 
         mac => PAULMAC, 
         permanentFlag => 1
      );

      #Inet::Table::IFACE->set_ip($name, '192.168.7.1');
      #Inet::Table::IFACE->set_mac($name, PAULMAC);
      #Inet::Table::IFACE->set_packet_type($name, 'ETHERNET');
      Inet::Table::IFACE->add(
         interface => $name,
         ip => '192.168.7.1',
         mac => PAULMAC,
         packetType => 'ETHERNET' 
      );

      Inet::Table::ROUTE->add(
         network => '192.168.7.1', 
         gateway => '0.0.0.0', 
         interface => 'lo'
      );
      Inet::Table::ROUTE->add(
         network => '192.168.7', 
         gateway => '0.0.0.0', 
         interface => $name
      );
      Inet::Table::ROUTE->add(
         network => '0.0.0.0', 
         gateway => '192.168.7.100', 
         interface => $name
      );

   } elsif ($sel == 2) {
      
      # Mary
      Inet::Collection::SYSTEM->set_name("Mary");
      Inet::Collection::SYSTEM->set_type('HOST');

      $host = 'localhost';
      $port = 6072;

      # no parameter checking is performed on this user data :(

      # ================ PORT =================

      $name = "eth0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      # note, Ethernet, IP , ICMP are set up in Inet::Plant
   
      Inet::Table::ARP->add(
         ip => '192.168.7.3', 
         mac => MARYMAC, 
         permanentFlag => 1
      );

      #Inet::Table::IFACE->set_ip($name, '192.168.7.3');
      #Inet::Table::IFACE->set_mac($name, MARYMAC);
      #Inet::Table::IFACE->set_packet_type($name, 'ETHERNET');
      Inet::Table::IFACE->add(
         interface => $name,
         ip => '192.168.7.3',
         mac => MARYMAC,
         packetType => 'ETHERNET' 
      );

      Inet::Table::ROUTE->add(
         network => '192.168.7.3', 
         gateway => '0.0.0.0', 
         interface => 'lo'
      );
      Inet::Table::ROUTE->add(
         network => '192.168.7', 
         gateway => '0.0.0.0', 
         interface => $name
      );
      Inet::Table::ROUTE->add(
         network => '0.0.0.0', 
         gateway => '192.168.7.100', 
         interface => $name
      );

   } elsif ($sel == 3) {

      # Router 
      Inet::Collection::SYSTEM->set_name("Router");
      Inet::Collection::SYSTEM->set_type('HOST');
      Inet::Collection::SYSTEM->set_routerFlag(TRUE);

      $host = 'localhost';
      $port = 5072;

      # no parameter checking is performed on this user data :(

      # ================ PORT =================

      $name = "eth0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);
   
      Inet::Table::ARP->add(
         ip => '192.168.6.100', 
         mac => RMAC0, 
         permanentFlag => 1
      );

      #Inet::Table::IFACE->set_ip($name, '192.168.6.100');
      #Inet::Table::IFACE->set_mac($name, RMAC0);
      #Inet::Table::IFACE->set_packet_type($name, 'ETHERNET');
      Inet::Table::IFACE->add(
         interface => $name,
         ip => '192.168.6.100',
         mac => RMAC0,
         packetType => 'ETHERNET' 
      );

      Inet::Table::ROUTE->add(
         network => '192.168.6.100', 
         gateway => '0.0.0.0', 
         interface => 'lo'
      );
      Inet::Table::ROUTE->add(
         network => '192.168.6', 
         gateway => '0.0.0.0', 
         interface => $name
      );

      $port = 6070;

      # no parameter checking is performed on this user data :(

      # ================ PORT =================

      $name = "eth1";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      Inet::Table::ARP->add(
         ip => '192.168.7.100', 
         mac => RMAC1, 
         permanentFlag => 1
      );

      #Inet::Table::IFACE->set_ip($name, '192.168.7.100');
      #Inet::Table::IFACE->set_mac($name, RMAC1);
      #Inet::Table::IFACE->set_packet_type($name, 'ETHERNET');
      Inet::Table::IFACE->add(
         interface => $name,
         ip => '192.168.7.100',
         mac => RMAC1,
         packetType => 'ETHERNET' 
      );

      Inet::Table::ROUTE->add(
         network => '192.168.7.100', 
         gateway => '0.0.0.0', 
         interface => 'lo'
      );
      Inet::Table::ROUTE->add(
         network => '192.168.7', 
         gateway => '0.0.0.0',
         interface => $name
      );
   }

   # All hosts require the following tasks.

   # ========== IFACE CONTROLLER  ============

      $name = "IfaceCon";
      Tosf::Table::TASK->new(
         name => $name,
         periodic => FALSE,
         fsm => Inet::Fsm::IfaceCon->new(
            taskName => $name,
         )
      );
      Tosf::Table::TASK->reset($name);

   # ============== STREAM CONTROLLER  ==============

   $name = "s0";
   Inet::Table::IFACE->set_packetType($name, 'STREAM');
   Tosf::Table::TASK->new(
      name => $name,
      periodic => TRUE,
      period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
      fsm => Inet::Fsm::StreamCon->new(
         taskName => $name,
         ifaceName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   # ============== SHELL  ==============

   $name = "Shell";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => inetApp::Fsm::Shell->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   # ============== APPLICATIONS  ==============

   $name = "Inc";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => inetApp::Fsm::Inc->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   $name = "Incd";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => inetApp::Fsm::Incd->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   $name = "Render";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => inetApp::Fsm::Render->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   $name = "Renderd";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => inetApp::Fsm::Renderd->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   # ================ HEARTBEAT =================

   $name = "HBeat";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => TRUE,
      period => Tosf::Executive::TIMER->s2t(20),
      fsm => Inet::Fsm::HBeat->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);


   # ================ Arp Table Management =================

   $name = "ArpTablePurge";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => TRUE,
      period => Tosf::Executive::TIMER->s2t(1),
      fsm => Inet::Fsm::ArpTablePurge->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

}

1;
