package inetApp::Plant::HOST;
#================================================================--
# File Name    : HOST.pm
#
# Purpose      : Plant set-up for HOST 
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;
use constant TRUE => 1;
use constant FALSE => 0;
use constant MAXSEL => 2 ;
use constant IFACEPOLLFREQ => 0.1;

sub start {
   system('clear');

   my $inp;
   my $sel = -1;

   while (($sel < 0) || ($sel > MAXSEL))  {

      print ("\nHOST Boot Menu\n\n");
      print ("\tLAN 0\n");
      print ("\tMAN 1\n");
      print ("\tWAN 2\n");
      print ("\nEnter selection (CTRL C to exit) ");
      $inp = <>;
      chop($inp);
      if (($inp =~ m/\d/) && (length($inp) == 1)) {
         $sel = int($inp);
      }

   }

   my $taskName;
   my $nicName;
   my $devName;

   if ($sel == 0) {
      # LAN 
      inetApp::Plant::LAN->start();
   } elsif ($sel == 1) {
      # MAN 
      inetApp::Plant::MAN->start();
   } elsif ($sel == 2) {
      # WAN 
      inetApp::Plant::WAN->start();
   }    

}

1;
