package Rube::Table::RUBEG;
#================================================================--
# File Name    : Table/RUBEG.pm
#
# Purpose      : table of RUBEG records
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

my %table;
my $max = 0;

sub delete {
   my $pkg = shift @_;
   my $mac = shift @_; #primary key

   if (!defined($mac)) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::RUBEG->delete"));
   }

   if (!exists($table{$mac})) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::RUBEG->delete"));
   }

   $max = $max-1;

   delete($table{$mac});

}

sub get_keys {

   return keys(%table);
}

sub add {
   my $pkg = shift @_;
   my $mac = shift @_; #primary key
   my $time = shift @_;

   if (!defined($mac) || (!defined($time))) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::RUBEG->add"));
   }

   if (!exists($table{$mac})) {
      $table{$mac} = Rube::Record::Rubeg->new();
      $table{$mac}->set_time($time);
      $table{$mac}->set_turn($max);
      $max = $max + 1
   }

}

sub find_turn {
   my $pkg = shift @_;
   my $turn = shift @_;

   if (!defined($turn)) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::RUBEG->find_turn"));
   }

   my $key;

   foreach $key (keys(%table)) {
      if ($table{$key}->get_turn() == $turn) {
         return $key;
      }
   }
   return undef;
}

sub get_turn {
   my $pkg = shift @_;
   my $mac = shift @_;

   if (!defined($mac)) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::RUBEG->get_turn"));
   }

   if (exists($table{$mac})) {
      return $table{$mac}->get_turn();
   } else {
      return undef;
   } 
}

sub set_total {
   my $pkg = shift @_;
   my $mac = shift @_;
   my $t = shift @_;

   if (!defined($mac) || (!defined($t)) || (!exists($table{$mac})) ) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::RUBEG->set_total"));
   }


   $table{$mac}->set_total($t);
}

sub get_total {
   my $pkg = shift @_;
   my $mac = shift @_;

   if (!defined($mac)) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::RUBEG->get_total"));
   }
   if (exists($table{$mac})) {
      return $table{$mac}->get_total();
   } else {
      return undef;
   } 
}

sub set_time {
   my $pkg = shift @_;
   my $mac = shift @_;
   my $t = shift @_;

   if (!defined($mac) || (!defined($t)) ||  (!exists($table{$mac})) ) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::RUBEG->set_time"));
   }


   $table{$mac}->set_time($t);
}

sub get_time {
   my $pkg = shift @_;
   my $mac = shift @_;

   if (!defined($mac)) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::RUBEG->get_time"));
   }
   if (exists($table{$mac})) {
      return $table{$mac}->get_time();
   } else {
      return undef;
   } 
}

sub clear {
   my $self = shift @_;

   my $key;

   foreach $key (keys(%table)) {
      delete($table{$key});
      $max = $max-1;
   } 
}

sub dumps {
   my $self = shift @_;

   my $key;
   my $s = '';

   foreach $key (keys(%table)) {
      $s = $s . "Rubeg entry: $key ";
      $s = $s . $table{$key}->dumps();
      $s = $s . "\n";
   } 
   return $s;
}

sub dump {
   my $self = shift @_;

   my $key;

   foreach $key (keys(%table)) {
      print ("Name: $key \n");
      $table{$key}->dump();
      print ("\n");
   } 
}

1;
