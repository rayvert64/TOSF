package Rube::Table::SYSTEMMSG;
#================================================================--
# File Name    : Table/SYSTEMMSG.pm
#
# Purpose      : table of system messages records
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

my %table;

# FSM name primary key
#
sub get_keys {

   return keys(%table);
}

sub add {
   my $pkg = shift @_;
   my %params = @_;

   if (!defined($params{name})) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::SYSTEMMSG->add"));
   }

   my $name = $params{name};

   if (!exists($table{$name})) {
      $table{$name} = Rube::Record::SystemMsg->new();
   }

   if (defined($params{permanentFlag})) {
      $table{$name}->set_bootupFlag($params{permanentFlag});
   }

   if (defined($params{requestFlag})) {
      $table{$name}->set_requestFlag($params{requestFlag});
   }

}

sub delete {
   my $pkg = shift @_;
   my $name = shift @_; #primary key

   if (!defined($name)) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::SYSTEMMSG->delete"));
   }

   if (!exists($table{$name})) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::SYSTEMMSG->delete"));
   }

   delete($table{$name});

}

sub get_siz {
   my $pkg = shift @_;
   my $name = shift @_;

   if (!defined($name)) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::SYSTEMMSG->get_siz"));
   }

   if (exists($table{$name})) {
      return $table{$name}->get_siz();
   } else {
      return undef;
   } 
}

sub get_requestFlag {
   my $pkg = shift @_;
   my $name = shift @_;

   if (!defined($name)) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::SYSTEMMSG->get_requestFlag"));
   }

   if (exists($table{$name})) {
      return $table{$name}->get_requestFlag();
   } else {
      return undef;
   } 
}

sub set_requestFlag {
   my $pkg = shift @_;
   my $name = shift @_;
   my $f = shift @_;

   if (!defined($name) || (!defined($f))) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::SYSTEMMSG->set_requestFlag"));
   }

   if (!exists($table{$name})) {
      $table{$name} = Rube::Record::SystemMsg->new();
   } 

   $table{$name}->set_requestFlag($f);
}

sub set_bootupFlag {
   my $pkg = shift @_;
   my $name = shift @_;
   my $f = shift @_;

   if (!defined($name) || (!defined($f))) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::SYSTEMMSG->set_bootupFlag"));
   }

   if (!exists($table{$name})) {
      $table{$name} = Rube::Record::SystemMsg->new();
   } 

   $table{$name}->set_bootupFlag($f);
}

sub get_bootupFlag {
   my $pkg = shift @_;
   my $name = shift @_;

   if (!defined($name)) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::SYSTEMMSG->get_bootupFlag"));
   }

   if (exists($table{$name})) {
      return $table{$name}->get_bootupFlag();
   } else {
      return undef;
   } 
}

sub enqueue {
   my $pkg = shift @_;
   my $name = shift @_;
   my $m = shift @_;

   if (!defined($name) || (!defined($m))) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::SYSTEMMSG->enqueue"));
   }

   if (!exists($table{$name})) {
      $table{$name} = Rube::Record::SystemMsg->new();
   } 

   $table{$name}->enqueue($m);
}

sub dequeue {
   my $pkg = shift @_;
   my $name = shift @_;

   if (!defined($name)) {
      die(Tosf::Exception::Trap->new(name => "Rube::Table::SYSTEMMSG->dequeue"));
   }

   if (exists($table{$name})) {
      return $table{$name}->dequeue();
   } else {
      return undef;
   } 
}

sub dumps {
   my $self = shift @_;

   my $key;
   my $s = '';

   foreach $key (keys(%table)) {
      $s = $s . "Fsm: $key\t";
      $s = $s . $table{$key}->dumps();
      $s = $s . "\n";
   } 
   return $s;
}

sub dump {
   my $self = shift @_;

   my $key;

   foreach $key (keys(%table)) {
      print ("Fsm: $key \n");
      $table{$key}->dump();
      print ("\n");
   } 
}

1;
