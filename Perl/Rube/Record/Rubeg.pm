package Rube::Record::Rubeg;
#================================================================--
# File Name    : Record/Rubeg.pm
#
# Purpose      : implements Rubeg record
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

my $ttl = 0;
my $turn = 0;
my $total = 0;

sub  new {
   my $class = shift @_;

   my $self = {
       turn => $turn,
       ttl => $ttl,
       total => $total
   };
     
   bless ($self, $class);
   return $self;
}

sub get_total {
   my $self = shift @_;
   
   return $self->{total};
}

sub set_total {
   my $self = shift @_;
   my $t = shift @_;
 
   $self->{total} = $t;
   return;
}

sub get_turn {
   my $self = shift @_;
   
   return $self->{turn};
}

sub set_turn {
   my $self = shift @_;
   my $t = shift @_;
 
   $self->{turn} = $t;
   return;
}

sub get_time {
   my $self = shift @_;
   
   return $self->{ttl};
}

sub set_time {
   my $self = shift @_;
   my $t = shift @_;
 
   $self->{ttl} = $t;
   return;
}

sub dumps {
   my $self = shift @_;

   my $s = '';

   $s = $s . "Rubeg: $self->{turn}  TTL: $self->{ttl}";

   return $s;
}

sub dump {
   my $self = shift @_;

   print ("Rubeg: $self->{turn} TTL: $self->{ttl}\n");
   
   return;
}

1;
