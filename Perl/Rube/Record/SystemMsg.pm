package Rube::Record::SystemMsg;
#================================================================--
# File Name    : Record/SystemMsg.pm
#
# Purpose      : implements System messages record
#
# Author       : Philippe Boutin , Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;


my $bootupFlag = 0;
my $requestFlag = 0;

sub  new {
   my $class = shift @_;

   my $self = {
      bootupFlag => $bootupFlag,
      requestFlag => $requestFlag,
      mqueue => Tosf::Collection::Queue->new(),
   };
                
   bless ($self, $class);
   return $self;
}

sub enqueue {
   my $self = shift @_;
   my $p = shift @_;
   
   $self->{mqueue}->enqueue($p);
}

sub get_num_msg {
   my $self = shift @_;
   
   return $self->{mqueue}->get_siz();
}

sub dequeue {
   my $self = shift @_;
   
   return ($self->{mqueue}->dequeue());
}

sub get_bootupFlag {
   my $self = shift @_;
   
   return $self->{bootupFlag};
}

sub set_bootupFlag {
   my $self = shift @_;
   my $f = shift @_;
 
   $self->{bootupFlag} = $f;
   return;
}

sub get_requestFlag {
   my $self = shift @_;
   
   return $self->{requestFlag};
}

sub set_requestFlag {
   my $self = shift @_;
   my $f = shift @_;
 
   $self->{requestFlag} = $f;
   return;
}

sub dumps {
   my $self = shift @_;

   my $s = '';

   $s = $s . "Bootup Flag: $self->{bootupFlag} Req Flag $self->{requestFlag}";
   $s = $s . " Queue Size: " . $self->{mqueue}->get_siz();
   $s = $s . "\n\t" . $self->{mqueue}->dumps();

   return $s;
}


sub dump {
   my $self = shift @_;

   print ("Name: $self->{name} Bootup Flag: $self->{bootupFlag} Req Flag $self->{requestFlag}");
   $self->{mqueue}->dump();
   return;
}

1;
