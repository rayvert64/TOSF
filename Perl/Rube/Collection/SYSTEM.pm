package Rube::Collection::SYSTEM;
#================================================================--
# File Name    : SYSTEM.pm
#
# Purpose      : implements SYSTEM ADT
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

use AnyEvent;

my $name = '';
my $type = '';
my $boot_time = 'unknown';
my $version = 'unknown';
my $routerFlag = 0; 
my $gpkt = Rube::Packet::Generic->new();

sub set_version {
   my $pkg = shift @_;
   my $ver = shift @_;
   
   $version = $ver;

}

sub get_version {
   my $pkg = shift @_;

   return $version;
}

sub set_boot_time {
   my $pkg = shift @_;
   my $tme = shift @_;
   
   $boot_time = $tme;

   return;
}

sub get_boot_time {
   my $pkg = shift @_;

   return $boot_time;
}

sub set_name {
   my $pkg = shift @_;
   my $nme = shift @_;
   
   $name = $nme;

   return;
}

sub get_name {
   my $pkg = shift @_;

   return $name;
}

sub set_type {
   my $pkg = shift @_;
   my $t = shift @_;
   
   $type = $t;

   return;
}

sub get_type {
   my $pkg = shift @_;

   return $type;
}

sub set_routerFlag {
   my $pkg = shift @_;
   my $f = shift @_;

   $routerFlag= $f;

   return;
}

sub get_routerFlag {
   my $pkg = shift @_;

   return $routerFlag;
}

sub printk {
   my $pkg = shift @_;
   my $string = shift @_;
   #Rube::Table::IFACE->enqueue_packet(Rube::Table::IFACE->get_streamIface(), $string);

   return;
}

sub dumps {
   my $self = shift @_;

   my $s = '';

   $s = $s . "Name: $name \n";
   $s = $s . "Boot Time: $boot_time \n";
   $s = $s . "Inet Version: $version \n";
   $s = $s . "Router Flag: $routerFlag \n";

   return $s;
}

sub dump {
   my $self = shift @_;
   
   print ("Name: ", $name, "\n");
   print ("Boot Time: ", $boot_time, "\n");
   print ("Inet Version: ", $version , "\n");
   print ("Router Flag: ", $routerFlag , "\n");

   return;
}

1;
