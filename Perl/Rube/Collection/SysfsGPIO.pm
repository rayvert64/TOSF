package Rube::Collection::SysfsGPIO;                                                      
#
# Package taken from https://raspberrypi.stackexchange.com/questions/41014/gpio-callbacks-in-any-language
#

use strict;                                                             
use warnings FATAL => qw(all);                                          

use Fcntl qw(SEEK_SET);                                                 
use Time::HiRes;                                                        

our $SysfsPath = "/sys/class/gpio";                                     

use constant {                                                          
    INPUT      => "in\n",                                               
    OUTPUT     => "out\n",                                              
    HIGH       => "1\n",                                                
    LOW        => "0\n",                                                
    POSEDGE    => "rising\n",                                           
    NEGEDGE    => "falling\n",                                          
    BOTH_EDGES => "both\n",                                             
    NO_EDGE    => "none\n"                                              
};                                                                      


sub openAndWrite {                                                      
    open(my $fh, '>', shift) or return 0;                               
    print $fh shift;                                                    
    close $fh;                                                          
    return 1;                                                           
}                                                                       


sub openAndRead {                                                       
    open(my $fh, '<', shift) or return undef;                           
    my $x = "";                                                         
    sysread($fh, $x, 1024);                                             
    close $fh;                                                          
    return $x;                                                          
}                                                                       


sub new {                                                               
    my $class = shift @_;
    my %params = @_;

    my $self = {
        number => my $number,
        direction => my $direction,
        edge => my $edge,
        active => my $active,
        path => my $path,
        handle => my $handle,
    };                                                                  

    if (defined($params{number})) {
        $self->{number} = $params{number};
        $self->{path} = "$SysfsPath/gpio".$params{number};
    } else {
        die(Tosf::Exception::Trap->new(name => "Collection::SysfsGPIO->new  pin could not be set up"));
    }   
 
    if (defined($params{direction})) {
        $self->{direction} = $params{direction};
    } else {
        die(Tosf::Exception::Trap->new(name => "Collection::SysfsGPIO->new  pin could not be set up"));
    }
    
    if (defined($params{edge})) {
        $self->{edge} = $params{edge};
    } else {
        die(Tosf::Exception::Trap->new(name => "Collection::SysfsGPIO->new  pin could not be set up"));
    }   
    
    if (defined($params{active})) {
        $self->{active} = $params{active};
    } else {
        die(Tosf::Exception::Trap->new(name => "Collection::SysfsGPIO->new  pin could not be set up"));
    }

    bless $self, $class;                                                
    
    return undef if (                                                   
        !-e $self->{path}                                               
        && !openAndWrite (                                              
            "$SysfsPath/export",                                        
            "$self->{number}\n"                                         
        )                                                               
    );
sleep(1);
print("Exported\n");
    return undef if (                                                   
        (                                                               
            $self->{direction} &&                                       
            !$self->setDirection($self->{direction})                    
        ) || (                                                          
            !$self->getDirection ||                                     
            !$self->openValue                                           
        )                                                               
    );  
sleep(1);
print("Dir\n");
    return undef if (                                                   
        (                                                               
            $self->{edge} &&                                           
            !$self->setEdge($self->{edge})                               
        ) || !$self->getEdge                                            
    );  
sleep(1);
print("Edge\n");
    return undef if (                                                   
        (                                                               
            $self->{active} &&                                          
            !$self->setActive($self->{active})                          
        ) || !$self->getActive                                          
    );                                                              
print("Active$self\n");
    return $self;                                                       
}                                                                       


sub DESTROY {                                                           
    my $self = shift;                                                   
    if ($self->{handle}) {                                              
        close $self->{handle};                                          
        $self->{handle} = undef;                                        
    }                                                                   
}                                                                       


sub close {                                                             
    my $self = shift;                                                   
    #    return 0 if !$self->setDirection(INPUT);                            
    if ($self->{handle}) {                                              
        close $self->{handle};                                          
        $self->{handle} = undef;                                        
    }                                                                   
    return 0 if !openAndWrite (                                         
        "$SysfsPath/unexport",                                          
        "$self->{number}\n"                                             
    );                                                                  
    return 1;                                                           
}                                                                       


sub checkBounceTime {                                                   
    my $self = shift;                                                   
    return undef if !$self->{bouncetime};                               
    if ($self->{lastbounce}) {                                          
        my $diff = $self->{bouncetime} -                                
            (Time::HiRes::time() - $self->{lastbounce});                
        return $diff if $diff > 0;                                      
    }                                                                   
    $self->{lastbounce} = Time::HiRes::time();                          
    return 0;                                                           
}                                                                       


sub getActive {                                                         
    my $self = shift;                                                   
    my $alow = openAndRead("$self->{path}/active_low");                 
    return undef if !$alow;                                             
    if ($alow eq LOW) {                                                 
        $self->{active} = HIGH                                          
    } else {                                                            
        $self->{active} = LOW                                           
    }                                                                   
    return $self->{active};                                             
}                                                                       


sub getEdge {                                                           
    my $self = shift;                                                   
    $self->{edge} = openAndRead("$self->{path}/edge");                  
    return $self->{edge};                                               
}                                                                       


sub getDirection {                                                      
    my $self = shift;                                                   
    $self->{direction} = openAndRead("$self->{path}/direction");        
    return $self->{direction};                                          
}                                                                       


sub getValue {                                                          
    my $self = shift;                                                   
    return undef if (                                                   
        sysread($self->{handle}, $self->{lastValue}, 3) <= 0            
    );                                                                  
    seek($self->{handle}, 0, SEEK_SET);                                 
    return $self->{lastValue};                                          
}                                                                       


sub setActive {                                                         
    my $self = shift;                                                   
    $self->{active} = shift;                                            
    my $alow = LOW;                                                     
    $alow = HIGH if $self->{active} eq LOW;                             
    return openAndWrite (                                               
        "$self->{path}/active_low",                                     
        $alow                                                           
    );                                                                  
}                                                                       


sub setEdge {                                                           
    my $self = shift;                                                   
    $self->{edge} = shift;                                              
    return openAndWrite (                                               
        "$self->{path}/edge",                                           
        $self->{edge}                                                   
    );                                                                  
}                                                                       


sub setDirection {                                                      
    my $self = shift;                                                   
    $self->{direction} = shift;                                         
    return openAndWrite (                                               
        "$self->{path}/direction",                                      
        $self->{direction}                                              
    );                                                                  
}                                                                       

sub setValue {
    my $self = shift @_;
    my $val = shift @_;
    return undef if ($val != 0 && $val !=1);
    return openAndWrite (
        "$self->{path}/value",
        $val
    );
}

sub getHandle {
    my $self = shift;
    my $fh = FileHandle->new();
    $fh->open("$self->{path}/value");

    return $fh;
}

sub openValue {                                                         
    my $self = shift;                                                   
    if ($self->{handle}) {                                              
        CORE::close $self->{handle};                                    
        $self->{handle} = undef;                                        
    }                                                                   
    if ($self->{direction} eq OUTPUT) {                                 
        return 0 if !open($self->{handle}, '>', "$self->{path}/value"); 
        return 1;                                                       
    } elsif ($self->{direction} eq INPUT) {                             
        return 0 if !open($self->{handle}, '<', "$self->{path}/value"); 
        return 1;                                                       
    }                                                                   
    return 0;                                                           
}                                             

1;
