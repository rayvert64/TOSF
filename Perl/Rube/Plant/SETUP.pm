package Rube::Plant::SETUP;
#================================================================--
# File Name    : SETUP.pm
#
# Purpose      : Plant set-up 
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;
use constant TRUE => 1;
use constant FALSE => 0;

sub start {
  
   my $name;
   my $sem;

   $name = "RubeG";
   $sem = "RubeGSm";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      run => FALSE,
      fsm => Rube::Fsm::RubeG->new(
         taskName => $name,
         taskSem => $sem,
      )
   );
   Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
   Tosf::Table::TASK->reset($name);
   
   #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);

   #   $name = "Arp";
   #   Tosf::Table::TASK->new(
   #      name => $name,
   #      periodic => FALSE,
   #      run => TRUE,
   #      fsm => Rube::Fsm::Arp->new(
   #         taskName => $name
   #      )
   #   );
   #
   #   Tosf::Table::TASK->reset("Arp");
   #
   #   $name = "Ip";
   #   Tosf::Table::TASK->new(
   #      name => $name,
   #      periodic => FALSE,
   #      run => FALSE,
   #      fsm => Rube::Fsm::Ip->new(
   #         taskName => $name
   #      )
   #   );
   #
   #   Tosf::Table::TASK->reset("Ip");
   #
   #   $name = "Icmp";
   #   Tosf::Table::TASK->new(
   #      name => $name,
   #      periodic => FALSE,
   #      run => FALSE,
   #      fsm => Rube::Fsm::Icmp->new(
   #         taskName => $name
   #      )
   #   );
   #
   #   Tosf::Table::TASK->reset("Icmp");
   #
   #   $name = "Udp";
   #   Tosf::Table::TASK->new(
   #      name => $name,
   #      periodic => FALSE,
   #      run => FALSE,
   #      fsm => Rube::Fsm::Udp->new(
   #         taskName => $name
   #      )
   #   );
   #
   #   Tosf::Table::TASK->reset("Udp");

}

1;
