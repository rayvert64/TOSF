This document specifies the behaviour  of Inet's  aperiodic tasks.
Each task receives a generic packet as input (work unit for the current
task) and may generate a generic packet as output (work unit for some
other task). Packet fields are referenced throughout the document. For
example, $interface is the interface field in a generic packet.  Each task
is specified using the following headings:

Opcode:  numeric value  dentifies the service the task offers
Input:   a work unit in the form of a generic packet with
         certain assigned packet fields
Action:  an overview of the actions necessary to provide the service
Outout:  a work unit in the form of a generic packet with
         certain assigned packet fields


Ethernet
========

Opcode:  0
Input:   $msg -> Ethernet packet x
Action:  if x's destination mac address is a mac address
             assigned to one of my interfaces {
             if the x's proto field is IP {
               extract Ip packet y from x
               set $opcode to 0
               signal Ip task
            }
         } else drop x
Output:  $opcode <- 0
         $msg <- y 
           

Opcode:  1
Input:   $msg -> Ip packet x
         $src_ip 
         $dest_ip 
Action:  create and populate an Ethernet packet y
         set y's payload to x 
         set y's source mac address to $src_ip's mac
         set y's destination mac address to $dest_ip's mac
         set $opcode to 0
         signal the IfaceCon  task.
Outout:  $opcode <- 0
         $msg <- y 
           
Ip
== 

Opcode:  0
Input:   $msg -> Ip packet x
Action:  set $src_ip to x's source ip
         set $dest_ip to x's destination ip
         determine x's routed interface i
         determine x's routed gateway  g
         set $interface to i 
         set $gateway to g 
         if i is localhost {
            if x's proto field is ICMP {
            extract Ip packet y from x
            set $opcode to 1
            signal Icmp task
         } else drop the packet
            
Output:  $opcode <- 1
         $msg <- y 
           

Opcode:  1
Input:   $msg -> Ip packet x
         $src_ip 
         $dest_ip 
Action:  Create and populate an Ethernet packet y.
         Set y's payload to x. Set y's source mac address 
         to $src_ip's mac.  Set y's destination 
         mac address to $dest_ip's mac.
         Signal the IfaceCon  task.
Outout:  $opcode <- 0
         $msg <- y 
           

Icmp
====
Opcode = 0  

Input:  Generic packet containing  message as payload.
Output: Generic packet containing an Icmp packet as payload.
Action: Based on the Generic packet's payload message, create an
        echo-request, echo-reply or time-exceeded Icmp packet.  Add Icmp
        packet as payload to a Generic packet and advance the packet
        to Ip.

Opcode = 1 

Input:  Generic packet containing an Icmp packet as payload.
Output: Generic Packet.
Action: De-multiplex Icmp packet to determine the packet's next handler.
        Add appropriate payload to a Generic packet and advance the
        packet to the next packet handler.
