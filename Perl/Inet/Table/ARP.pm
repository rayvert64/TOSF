package Inet::Table::ARP;
#================================================================--
# File Name    : Table/ARP.pm
#
# Purpose      : table of  Arp records
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

my %table;

# ip primary key
#
sub get_keys {

   return keys(%table);
}

sub add {
   my $pkg = shift @_;
   my %params = @_;

   if (!defined($params{ip})) {
      die(Tosf::Exception::Trap->new(name => "Inet::Table::ARP->add"));
   }

   my $ip = $params{ip};

   if (!exists($table{$ip})) {
      $table{$ip} = Inet::Record::Arp->new();
   }

   if (defined($params{mac})) {
      $table{$ip}->set_mac($params{mac});
   }

   if (defined($params{requestFlag})) {
      $table{$ip}->set_requestFlag($params{requestFlag});
   }

   if (defined($params{permanentFlag})) {
      $table{$ip}->set_permanentFlag($params{permanentFlag});
   }

   if (defined($params{time})) {
      $table{$ip}->set_time($params{time});
   }

}

sub delete {
   my $pkg = shift @_;
   my $ip = shift @_; #primary key

   if (!defined($ip)) {
      die(Tosf::Exception::Trap->new(name => "Inet::Table::ARP->delete"));
   }

   if (!exists($table{$ip})) {
      die(Tosf::Exception::Trap->new(name => "Inet::Table::ARP->delete"));
   }

   delete($table{$ip});

}

sub set_mac {
   my $pkg = shift @_;
   my $ip = shift @_;
   my $mac = shift @_;

   if (!defined($ip) || (!defined($mac))) {
      die(Tosf::Exception::Trap->new(name => "Inet::Table::ARP->set_mac"));
   }

   if (!exists($table{$ip})) {
      $table{$ip} = Inet::Record::Arp->new();
   } 

   $table{$ip}->set_mac($mac);
}

sub newSlice {
   my $pkg = shift @_;
   my $ip = shift @_;

   if (!defined($ip)) {
      die(Tosf::Exception::Trap->new(name => "Inet::Table::ARP->newSlice"));
   }

   if (!exists($table{$ip})) {
      $table{$ip} = Inet::Record::Arp->new();
   } 

   $table{$ip}->newSlice();
}

sub get_mac {
   my $pkg = shift @_;
   my $ip = shift @_;

   if (!defined($ip)) {
      die(Tosf::Exception::Trap->new(name => "Inet::Table::ARP->get_mac"));
   }

   if (exists($table{$ip})) {
      return $table{$ip}->get_mac();
   } else {
      return undef;
   } 
}

sub get_siz {
   my $pkg = shift @_;
   my $ip = shift @_;

   if (!defined($ip)) {
      die(Tosf::Exception::Trap->new(name => "Inet::Table::ARP->get_siz"));
   }

   if (exists($table{$ip})) {
      return $table{$ip}->get_siz();
   } else {
      return undef;
   } 
}

sub get_requestFlag {
   my $pkg = shift @_;
   my $ip = shift @_;

   if (!defined($ip)) {
      die(Tosf::Exception::Trap->new(name => "Inet::Table::ARP->get_requestFlag"));
   }

   if (exists($table{$ip})) {
      return $table{$ip}->get_requestFlag();
   } else {
      return undef;
   } 
}

sub set_requestFlag {
   my $pkg = shift @_;
   my $ip = shift @_;
   my $f = shift @_;

   if (!defined($ip) || (!defined($f))) {
      die(Tosf::Exception::Trap->new(name => "Inet::Table::ARP->set_requestFlag"));
   }

   if (!exists($table{$ip})) {
      $table{$ip} = Inet::Record::Arp->new();
   } 

   $table{$ip}->set_requestFlag($f);
}

sub set_time {
   my $pkg = shift @_;
   my $ip = shift @_;
   my $t = shift @_;

   if (!defined($ip) || (!defined($t))) {
      die(Tosf::Exception::Trap->new(name => "Inet::Table::ARP->set_time"));
   }

   if (!exists($table{$ip})) {
      $table{$ip} = Inet::Record::Arp->new();
   } 

   $table{$ip}->set_time($t);
}

sub get_time {
   my $pkg = shift @_;
   my $ip = shift @_;

   if (!defined($ip)) {
      die(Tosf::Exception::Trap->new(name => "Inet::Table::ARP->get_time"));
   }

   if (exists($table{$ip})) {
      return $table{$ip}->get_time();
   } else {
      return undef;
   } 
}

sub set_permanentFlag {
   my $pkg = shift @_;
   my $ip = shift @_;
   my $f = shift @_;

   if (!defined($ip) || (!defined($f))) {
      die(Tosf::Exception::Trap->new(name => "Inet::Table::ARP->set_permanentFlag"));
   }

   if (!exists($table{$ip})) {
      $table{$ip} = Inet::Record::Arp->new();
   } 

   $table{$ip}->set_permanentFlag($f);
}

sub get_permanentFlag {
   my $pkg = shift @_;
   my $ip = shift @_;

   if (!defined($ip)) {
      die(Tosf::Exception::Trap->new(name => "Inet::Table::ARP->get_permanentFlag"));
   }

   if (exists($table{$ip})) {
      return $table{$ip}->get_permanentFlag();
   } else {
      return undef;
   } 
}

sub enqueue {
   my $pkg = shift @_;
   my $ip = shift @_;
   my $m = shift @_;

   if (!defined($ip) || (!defined($m))) {
      die(Tosf::Exception::Trap->new(name => "Inet::Table::ARP->enqueue"));
   }

   if (!exists($table{$ip})) {
      $table{$ip} = Inet::Record::Arp->new();
   } 

   $table{$ip}->enqueue($m);
}

sub dequeue {
   my $pkg = shift @_;
   my $ip = shift @_;

   if (!defined($ip)) {
      die(Tosf::Exception::Trap->new(name => "Inet::Table::ARP->dequeue"));
   }

   if (exists($table{$ip})) {
      return $table{$ip}->dequeue();
   } else {
      return undef;
   } 
}

sub dumps {
   my $self = shift @_;

   my $key;
   my $s = '';

   foreach $key (keys(%table)) {
      $s = $s . "Net: $key ";
      $s = $s . $table{$key}->dumps();
      $s = $s . "\n";
   } 
   return $s;
}

sub dump {
   my $self = shift @_;

   my $key;

   foreach $key (keys(%table)) {
      print ("Name: $key \n");
      $table{$key}->dump();
      print ("\n");
   } 
}

1;
