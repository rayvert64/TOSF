package Inet::Record::Arp;
#================================================================--
# File Name    : Record/Arp.pm
#
# Purpose      : implements Route record
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

use constant SLICE => 60;

my $mac = 'unknown';
my $permanentFlag = 0;
my $requestFlag = 0;
my $time = SLICE;

sub  new {
   my $class = shift @_;

   my $self = {
      mac => $mac,
      permanentFlag => $permanentFlag,
      requestFlag => $requestFlag,
      wqueue => Tosf::Collection::Queue->new(),
      time => $time
   };
                
   bless ($self, $class);
   return $self;
}

sub newSlice{
   my $self = shift @_;

   $self->{time} = SLICE;
   return;
}

sub get_mac {
   my $self = shift @_;
   
   if ($self->{mac} eq "unknown") {
      return(undef);
   } else {
      return $self->{mac};
   }
}

sub set_mac {
   my $self = shift @_;
   my $m = shift @_;
 
   $self->{mac} = $m;
   return;
}

sub enqueue {
   my $self = shift @_;
   my $p = shift @_;
   
   $self->{wqueue}->enqueue($p);
}

sub get_siz {
   my $self = shift @_;
   
   return $self->{wqueue}->get_siz();
}

sub dequeue {
   my $self = shift @_;
   
   return ($self->{wqueue}->dequeue());
}

sub get_permanentFlag {
   my $self = shift @_;
   
   return $self->{permanentFlag};
}

sub set_permanentFlag {
   my $self = shift @_;
   my $f = shift @_;
 
   $self->{permanentFlag} = $f;
   return;
}

sub get_requestFlag {
   my $self = shift @_;
   
   return $self->{requestFlag};
}

sub set_requestFlag {
   my $self = shift @_;
   my $f = shift @_;
 
   $self->{requestFlag} = $f;
   return;
}

sub get_time {
   my $self = shift @_;
   
   return $self->{time};
}

sub set_time {
   my $self = shift @_;
   my $m = shift @_;
 
   $self->{time} = $m;
   return;
}

sub dumps {
   my $self = shift @_;

   my $s = '';

   $s = $s . "Mac: $self->{mac} Perm Flag: $self->{permanentFlag} Req Flag $self->{requestFlag} Time out: $self->{time}";
   $s = $s . " Queue Size: " . $self->{wqueue}->get_siz();
   #$s = $s . "\n" . $self->{wqueue}->dumps();

   return $s;
}


sub dump {
   my $self = shift @_;

   print ("Mac: $self->{mac} Perm Flag: $self->{permanentFlag} Req Flag $self->{requestFlag} Time out: $self->{time}\n");
   $self->{wqueue}->dump();
   return;
}

1;
