package Inet::Plant::SETUP;
#================================================================--
# File Name    : SETUP.pm
#
# Purpose      : Plant set-up 
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;
use constant TRUE => 1;
use constant FALSE => 0;

sub start {
  
   my $name;

   $name = "Ethernet";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      run => TRUE,
      fsm => Inet::Fsm::Ethernet->new(
         taskName => $name
      )
   );

   Tosf::Table::TASK->reset("Ethernet");

   $name = "Arp";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      run => TRUE,
      fsm => Inet::Fsm::Arp->new(
         taskName => $name
      )
   );

   Tosf::Table::TASK->reset("Arp");

   $name = "Ip";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      run => FALSE,
      fsm => Inet::Fsm::Ip->new(
         taskName => $name
      )
   );

   Tosf::Table::TASK->reset("Ip");

   $name = "Icmp";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      run => FALSE,
      fsm => Inet::Fsm::Icmp->new(
         taskName => $name
      )
   );

   Tosf::Table::TASK->reset("Icmp");

   $name = "Udp";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      run => FALSE,
      fsm => Inet::Fsm::Udp->new(
         taskName => $name
      )
   );

   Tosf::Table::TASK->reset("Udp");

}

1;
