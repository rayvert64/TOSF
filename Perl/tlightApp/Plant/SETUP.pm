package tlightApp::Plant::SETUP;
#================================================================--
# File Name    : SETUP.pm
#
# Purpose      : Plant set-up 
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;
use constant TRUE => 1;
use constant FALSE => 0;
use constant MAXSEL => 3;

sub start {

   my $inp;
   my $sel = -1;

   system('clear');

   while (($sel < 0) || ($sel > MAXSEL))  {

      print ("\nBoot Menu\n\n");
      print("\tCentralized Traffic Light Controller  0\n");
      print("\tNetworked Traffic Light Controller  1\n");
      print("\tNetworked Traffic Lights  2\n");
      print("\tNetworked Traffic Light Hub  3\n");
      print ("\nEnter selection (CTRL C to exit) ");
      $inp = <>;
      chop($inp);
      if (($inp =~ m/\d/) && (length($inp) == 1)) {
         $sel = int($inp);
      }

   }

   system('clear');

   if ($sel == 0) {
      tlightApp::Plant::CLIGHTS->start();
      tlightApp::Plant::CCONTROL->start();
   } elsif ($sel == 1) {
      print("Sel 1 \n");
      exit();
   } elsif ($sel == 2) {
      print("Sel 2 \n");
      exit();
   } elsif ($sel == 3) {
      print("Sel 3 \n");
      exit();
   } elsif ($sel == 4) {
      print("Sel 4 \n");
      exit();
   }
}

1;
