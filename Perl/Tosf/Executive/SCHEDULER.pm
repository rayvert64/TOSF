package Tosf::Executive::SCHEDULER;
#================================================================--
# File Name    : SCHEDULER.pm
#
# Purpose      : EDF schedular
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;
#no warnings "experimental::smartmatch";

sub tick {

   my @keys = Tosf::Table::TASK->get_keys();
   my $k;
   my $periodic;
   my $inq;
   my $blocked;
   my $rtime;

   
   if (!Tosf::Collection::STATUS->get_cycleComplete()) {
      # need another time slice to finish scheduled tasks
      print("Warning: Clock Skew \n");	 
      return;
   } 
      

   Tosf::Table::SVAR->update_all();

   foreach $k (@keys) {

      $blocked = Tosf::Table::TASK->get_blocked($k);
      $rtime = Tosf::Table::TASK->get_resumeTime($k);
      $periodic = Tosf::Table::TASK->get_periodic($k); 

      if ($rtime == 0) {

         if ($blocked) { 

            if ($periodic) {
               $rtime = Tosf::Table::TASK->get_period($k);
            } else {
               $rtime = -1; 
            }

            Tosf::Table::TASK->set_resumeTime($k, $rtime);

            Tosf::Table::SEMAPHORE->resume(semaphore => Tosf::Table::TASK->get_blockingSem($k), task => $k); 
            #Tosf::Table::SVAR->update_all();
	    #Tosf::Table::SVAR->update($k); # only update the sval associated with the task
            $blocked = Tosf::Table::TASK->get_blocked($k);
            # uncomment the following to run the trace example Trace  
	    # print("Resume Task $k  Blocked $blocked Resume Time $rtime \n");

         }
      }

      if (!$blocked) {
        
         if ($periodic) {
            Tosf::Table::PQUEUE->enqueue('pTask', $k, $rtime);
             # uncomment the following to run the trace example Trace  
	     # print("Priority for task $k is $rtime \n");
         } else {
            $inq = Tosf::Table::QUEUE->is_member('apTask', $k);
            if (!$inq) {
               Tosf::Table::QUEUE->enqueue('apTask', $k);
            }
            # Note, this task is enqueued on a non priority queue
         }
      }


      $blocked = Tosf::Table::TASK->get_blocked($k);

      if ($rtime >= 1) {
         Tosf::Table::TASK->decrement_resumeTime($k);
      }
   }

   Tosf::Collection::STATUS->clear_cycleComplete();


}

1;
