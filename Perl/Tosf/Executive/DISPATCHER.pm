package Tosf::Executive::DISPATCHER;
#================================================================--
# File Name    : DISPATCHER.pm
#
# Purpose      : execute scheduled tasks
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

my $num_slots = 1;
my $key;
my $fsm;
my $res;
my $slot_count;
my $sleep_time;

sub set_num_slots {
   my $self = shift @_;
   $num_slots = shift @_;
}

sub tick {

   $slot_count = 0;

   while (Tosf::Table::PQUEUE->get_siz('pTask') && ($slot_count < $num_slots)) {
      $slot_count = $slot_count + 1;
      $key = Tosf::Table::PQUEUE->dequeue('pTask');
      Tosf::Collection::STATUS->set_currentExecutingTask($key);
      $fsm = Tosf::Table::TASK->get_fsm($key);
      Tosf::Table::TASK->set_nextState($key, $fsm->tick(Tosf::Table::TASK->get_nextState($key)));
   }

   # dequeue remaining periodic tasks and throw away
   while (Tosf::Table::PQUEUE->get_siz('pTask')) {
      $key = Tosf::Table::PQUEUE->dequeue('pTask');
   }

   while (Tosf::Table::QUEUE->get_siz('apTask') && ($slot_count < $num_slots+3)) {
      $slot_count = $slot_count + 1;
      $key = Tosf::Table::QUEUE->dequeue('apTask');
      Tosf::Collection::STATUS->set_currentExecutingTask($key);
      #print("\nDispatch $key\n\n");
      $fsm = Tosf::Table::TASK->get_fsm($key);
      Tosf::Table::TASK->set_nextState($key, $fsm->tick(Tosf::Table::TASK->get_nextState($key)));
   }

   # aperiodic tasks are executed round-robin

   Tosf::Collection::STATUS->set_cycleComplete();

}

1;
