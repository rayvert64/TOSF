package ipcApp::Plant::Sniffer;
#================================================================--
# File Name    : Sniffer.pm
#
# Purpose      : Sniffer for Soc (tnet packets)
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;
use constant TRUE => 1;
use constant FALSE => 0;

sub start {

   my $task;
   my $sem;

   #--------------------------------------------------
   $task = "CentralCon";
   $sem  = "CentralConSem";

   Tosf::Table::TASK->new(
      name => $task,
      periodic => TRUE,
      period => Tosf::Executive::TIMER->s2t(0.1),
      fsm => tlightApp::Fsm::CCON->new(
         taskName => $task,
         taskSem => $sem
      )
   );

   Tosf::Table::SEMAPHORE->add(name => $sem, max => 1);
   Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $task);

   #--------------------------------------------------


   # no parameter checking is performed on this user data :(

   print("*************************************************\n");
   print("*****            Sniffer Setup              *****\n");
   print("*************************************************\n \n");

   print("Enter Internet host name ");
   chomp($host = <>);
   print("Enter Internet port number: ");
   chomp($port = <>);

   Soc::Collection::SYSTEM->set_name("Peter_Sniffer");

   print("*************************************************\n");
   print("*****               End Setup               *****\n");
   print("*************************************************\n \n");


   # ================ PORT =================

   $task = "p0";
   $sem  = "p0Sem";

   Tosf::Table::TASK->new(
      name => $task, 
      periodic => TRUE, 
      period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
      fsm => Inet::Fsm::SocConC->new(
         taskName => $task,
         taskSem => $sem,
         ifaceName => $name,
         host => $host,
         port => $port
      )
   );

   $task = "Sniffer";
   $sem  = "SnifferSem";

   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => Inet::Fsm::Sniffer->new(
         taskName => $name,
         taskSem => $sem
      )
   );

   # All Soc systems require the following tasks.

   # ========== IFACE CONTROLLER  ============

   $task = "IfaceC";
   $sem  = "SnifferSem";
      $name = "IfaceCon";
      Tosf::Table::TASK->new(
         name => $name,
         periodic => FALSE,
         fsm => Inet::Fsm::IfaceCon->new(
            taskName => $name,
         )
      );

   # ============== STREAM CONTROLLER  ==============
  
   $name = "s0";
   Inet::Table::IFACE->set_packetType($name, 'STREAM');
   Tosf::Table::TASK->new(
      name => $name, 
      periodic => TRUE, 
      period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
      fsm => Inet::Fsm::StreamCon->new(
         taskName => $name,
         ifaceName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   $name = "Shell";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => inetApp::Fsm::Shell->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   # ================ HEARTBEAT =================
  
   $name = "HBeat";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => TRUE,
      period => Tosf::Executive::TIMER->s2t(20),
      fsm => Inet::Fsm::HBeat->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

}

1;
   #--------------------------------------------------

   my $k;

   foreach $k (@keys) {
      print("Resetting Task $k \n");
      Tosf::Table::TASK->reset($k);
   }

}

1;
