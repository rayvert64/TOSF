package ipcApp::Plant::NODE;
#================================================================--
# File Name    : Hub.pm
#
# Purpose      : Implement a 4 port hub
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;
use constant TRUE => 1;
use constant FALSE => 0;
use constant MAXSEL => 3 ;
use constant IFACEPOLLFREQ => 0.05;

sub start {

      }

   }

   my $host;
   my $port;
   my $name;
   my $port0;
   my $host4 = "none";
   my $port4 = "none";

   # no parameter checking is performed on this user data :(

   print("*************************************************\n");
   print("*****              Hub Setup                *****\n");
   print("*************************************************\n \n");
   print("Four consecutive Tcp Internet port numbers are required \n");
   print("on localhost for Hub ports p0 through p3 \n");
   print("Enter Internet port number for Hub p0: ");
   chomp($port0 = <>);
   print("Mapping \n");
   print("\t Hub port p0 to Internet port number ", $port0, "\n");
   print("\t Hub port p1 to Internet port number ", $port0 + 1, "\n");
   print("\t Hub port p2 to Internet port number ", $port0 + 2, "\n");
   print("\t Hub port p3 to Internet port number ", $port0 + 3, "\n \n");

   print("One Tcp Internet port number is required on a connected \n");
   print("host for Hub up-link port p4 \n");
   print("Enter Internet host name for Hub p4 (enter none to disable p4): ");
   chomp($host4 = <>);
   if ($host4 ne "none") {
      print("Enter Internet port number for Hub p4: ");
      chomp($port4 = <>);
      print("Mapping \n");
      print("\t Hub up-link port p4 to Internet port number ", $host4, ":", $port4, "\n \n");
   } else {
      print("Mapping \n");
      print("\t No up-link defined \n \n");
   }

   Inet::Collection::SYSTEM->set_name("Peter_Hub");
   Inet::Collection::SYSTEM->set_type('HUB');
   Inet::Collection::FLAG::->set_trace(FALSE);

   print("*************************************************\n");
   print("*****               End Setup               *****\n");
   print("*************************************************\n \n");

   my $name;

   # ================ PORTS =================

   $name = "p0";
   Inet::Table::IFACE->set_packetType($name, 'ETHERNET');
   Tosf::Table::TASK->new(
      name => $name, 
      periodic => TRUE, 
      period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
      fsm => Inet::Fsm::SocConS->new(
         taskName => $name,
         ifaceName => $name,
         port => $port0
      )
   );
   Tosf::Table::TASK->reset($name);

   $name = "p1";
   Inet::Table::IFACE->set_packetType($name, 'ETHERNET');
   Tosf::Table::TASK->new(
      name => $name, 
      periodic => TRUE, 
      period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
      fsm => Inet::Fsm::SocConS->new(
         taskName => $name,
         ifaceName => $name,
         port => ($port0 + 1)
      )
   );
   Tosf::Table::TASK->reset($name);

   $name = "p2";
   Inet::Table::IFACE->set_packetType($name, 'ETHERNET');
   Tosf::Table::TASK->new(
      name => $name, 
      periodic => TRUE, 
      period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
      fsm => Inet::Fsm::SocConS->new(
         taskName => $name,
         ifaceName => $name,
         port => ($port0 + 2)
      )
   );
   Tosf::Table::TASK->reset($name);

   $name = "p3";
   Inet::Table::IFACE->set_packetType($name, 'ETHERNET');
   Tosf::Table::TASK->new(
      name => $name, 
      periodic => TRUE, 
      period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
      fsm => Inet::Fsm::SocConS->new(
         taskName => $name,
         ifaceName => $name,
         port => ($port0 + 3)
      )
   );
   Tosf::Table::TASK->reset($name);

   if ($host4 ne 'none') {

      $name = "p4"; #(Up link)
      Inet::Table::IFACE->set_packetType($name, 'ETHERNET');
      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Inet::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host4,
            port => $port4
         )
      );
      Tosf::Table::TASK->reset($name);
   }

   $name = "Hub";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => Inet::Fsm::Hub->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);


   # All SYSTEM types require the following tasks.

   # ========== IFACE CONTROLLER  ============

   $name = "IfaceCon";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => Inet::Fsm::IfaceCon->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   # ============== STREAM CONTROLLER  ==============
  
   $name = "s0";
   Inet::Table::IFACE->set_packetType($name, 'STREAM');
   Tosf::Table::TASK->new(
      name => $name, 
      periodic => TRUE, 
      period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
      fsm => Inet::Fsm::StreamCon->new(
         taskName => $name,
         ifaceName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   $name = "Shell";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => inetApp::Fsm::Shell->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   # ================ HEARTBEAT =================
  
   $name = "HBeat";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => TRUE,
      period => Tosf::Executive::TIMER->s2t(20),
      fsm => Inet::Fsm::HBeat->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

}

1;
