package rubeApp::Plant::PYB;
#================================================================--
# File Name    : PYB.pm
#
# Purpose      : Plant set-up for PYB 
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;
use constant TRUE => 1;
use constant FALSE => 0;
use constant MAXSEL => 0;
use constant IFACEPOLLFREQ => 0.05;

sub start {
   system('clear');

   my $pins = TRUE;
   my $inp;
   my $sel = -1;

   while (($sel < 0) || ($sel > MAXSEL))  {

      print ("\nPYB Boot Menu\n\n");
      print ("\tPyboard 0\n");
      print ("\nEnter selection (CTRL C to exit) ");
      $inp = <>;
      chop($inp);
      if (($inp =~ m/\d/) && (length($inp) == 1)) {
         $sel = int($inp);
      }

   }

   my $name;
   my $sem;

   if ($sel == 0) {

      # Pyboard 
      Rube::Collection::SYSTEM->set_name("Pyboard");
      Rube::Collection::SYSTEM->set_type('PYB');
      Rube::Collection::FLAG->set_trace(FALSE);

      my $port = 42070;
      my $host = 'localhost';
      
      print("Enter Internet host name(or ip) for pyb0: ");
      chomp($host = <>);

      print("Enter Internet port number for Pyboard pyb0: ");
      chomp($port = <>);
      print("Mapping \n");
      print("\tPyboard port pyb0 to Internet port number ", $port, "\n\n");
      
      # no parameter checking is performed on this user data :(

      # ================ PORT =================
      $name = "pyb0";
      $sem = "pyb0Sm";
      Tosf::Table::TASK->new(
         name => $name,
         periodic => TRUE,
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Rube::Fsm::SocConC->new(
            taskName => $name,
            taskSem => $sem,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::SEMAPHORE->add(name => $sem, max => 1);
      #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
      Tosf::Table::TASK->reset($name);
     
      my $mac_addr = 0;

      while ($mac_addr == 0) {
         print("Enter Internet Mac Address for Pyboard pyb0(must be greater than 0): ");
         chomp($mac_addr = <>);
      }
      print("Mapping \n");
      print("\tPyboard port pyb0 to Internet port number ", $port, "\n");

      Rube::Table::IFACE->set_mac($name, $mac_addr);
      Rube::Table::IFACE->set_packetType($name, 'RUBEG');
      Rube::Table::ROUTE->set_route('1', '0.0.0.0', 'lo');
      Rube::Table::ROUTE->set_route('0.0.0.0', '0.0.0.0', $name);

   }   

   # All SYSTEM types require the following tasks.

   # ========== IFACE CONTROLLER  ============

   $name = "IfaceCon";
   $sem = "IfaceConSm";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      run => FALSE,
      fsm => Rube::Fsm::IfaceCon->new(
         taskName => $name,
         taskSem => $sem,
      )
   );
   Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
   #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
   Tosf::Table::TASK->reset($name);

   # ============== STREAM CONTROLLER  ==============

   $name = "s0";
   $sem = "s0Sm";
   Rube::Table::IFACE->set_packetType($name, 'STREAM');
   Tosf::Table::TASK->new(
      name => $name,
      periodic => TRUE,
      period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
      fsm => Rube::Fsm::StreamCon->new(
         taskName => $name,
         taskSem => $sem,
         ifaceName => $name,
      )
   );
   Tosf::Table::SEMAPHORE->add(name => $sem, max => 1);
   #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
   Tosf::Table::TASK->reset($name);

   $name = "Shell";
   $sem = "ShellSm";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => rubeApp::Fsm::Shell->new(
         taskName => $name,
         taskSem => $sem,
      )
   );
   Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
   #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
   Tosf::Table::TASK->reset($name);

   $name = "Timer";
   $sem = "TimerSm";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => rubeApp::Fsm::Timer->new(
         taskName => $name,
         taskSem => $sem,
      )
   );
   Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
   #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
   Tosf::Table::TASK->reset($name);  
 
   print("Is this machine a real RaspberryPi?(Y/n)");
   my $pinAnsw;
   chomp($pinAnsw = <>);
   if ($pinAnsw eq "n" || $pinAnsw eq "N") {
       $pins = FALSE;
   }
 
   $name = "Rube";
   $sem = "RubeSm";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      fsm => rubeApp::Fsm::Rube->new(
         taskName => $name,
         taskSem => $sem,
         pins => $pins
      )
   );
   Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
   #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
   Tosf::Table::TASK->reset($name);
   
   if ($pins == TRUE) {
       $name = "PinChk0";
       $sem = "PinChk0Sm";
       Tosf::Table::TASK->new(
          name => $name,
          periodic => FALSE,
          fsm => Rube::Fsm::PinChkCon->new(
             taskName => $name,
             taskSem => $sem,
             taskSignal => "Rube",
             taskSignalSem => "RubeSm",
             taskSignalOpcode => 8,
             resume => 1,
          ),
       );
       Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
       Tosf::Table::TASK->reset($name);
   }
   # ================ HEARTBEAT =================

   $name = "HBeat";
   $sem = "HBeatSm";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => TRUE,
      period => Tosf::Executive::TIMER->s2t(10),
      run => TRUE,
      fsm => Rube::Fsm::HBeat->new(
         taskName => $name,
         taskSem => $sem,
      )
   );
   Tosf::Table::SEMAPHORE->add(name => $sem, max => 1);
   Tosf::Table::TASK->reset($name);

}

1;
