package rubeApp::Plant::NODE;
#================================================================--
# File Name    : NODE.pm
#
# Purpose      : Plant set-up for NODE 
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;
use constant TRUE => 1;
use constant FALSE => 0;
use constant MAXSEL => 2;
use constant IFACEPOLLFREQ => 0.1;

sub start {
   system('clear');

   my $inp;
   my $sel = -1;

   while (($sel < 0) || ($sel > MAXSEL))  {

      print ("\nNODE Boot Menu\n\n");
      print ("\tSniffer 0\n");
      print ("\tHub 1\n");
      print ("\tSwitch 2\n");
      print ("\nEnter selection (CTRL C to exit) ");
      $inp = <>;
      chop($inp);
      if (($inp =~ m/\d/) && (length($inp) == 1)) {
         $sel = int($inp);
      }

   }

   my $host;
   my $port;
   my $name;
   my $sem;
   my $port0;
   my $host4 = "none";
   my $port4 = "none";

   if ($sel == 0) {

      # no parameter checking is performed on this user data :(

      print("*************************************************\n");
      print("*****            Sniffer Setup              *****\n");
      print("*************************************************\n \n");

      print("Enter Internet host name ");
      chomp($host = <>);
      print("Enter Internet port number: ");
      chomp($port = <>);

      Rube::Collection::SYSTEM->set_name("Peter_Sniffer");
      Rube::Collection::SYSTEM->set_type('SNIFFER');
      Rube::Collection::FLAG::->set_trace(FALSE);

      print("*************************************************\n");
      print("*****               End Setup               *****\n");
      print("*************************************************\n \n");


      # ================ PORT =================

      $name = "p0";
      $sem = "p0Sm";
      Rube::Table::IFACE->set_packetType($name, 'RUBEG');
      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         run => TRUE,
         fsm => Rube::Fsm::SocConC->new(
            taskName => $name,
            taskSem => $sem,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
      #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
      Tosf::Table::TASK->reset($name);

      $name = "Sniffer";
      $sem = "SnifferSm";
      Tosf::Table::TASK->new(
         name => $name,
         periodic => FALSE,
         run => FALSE,
         fsm => Rube::Fsm::Sniffer->new(
            taskName => $name,
            taskSem => $sem,
         )
      );
      Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
      #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
      Tosf::Table::TASK->reset($name);

   } elsif ($sel == 1) {

      # no parameter checking is performed on this user data :(

      print("*************************************************\n");
      print("*****              Hub Setup                *****\n");
      print("*************************************************\n \n");
      print("Four consecutive Tcp Internet port numbers are required \n");
      print("on localhost for Hub ports p0 through p3 \n");
      print("Enter Internet port number for Hub p0: ");
      chomp($port0 = <>);
      print("Mapping \n");
      print("\t Hub port p0 to Internet port number ", $port0, "\n");
      print("\t Hub port p1 to Internet port number ", $port0 + 1, "\n");
      print("\t Hub port p2 to Internet port number ", $port0 + 2, "\n");
      print("\t Hub port p3 to Internet port number ", $port0 + 3, "\n \n");

      print("One Tcp Internet port number is required on a connected \n");
      print("host for Hub up-link port p4 \n");
      print("Enter Internet host name for Hub p4 (enter none to disable p4): ");
      chomp($host4 = <>);
      if ($host4 ne "none") {
         print("Enter Internet port number for Hub p4: ");
         chomp($port4 = <>);
         print("Mapping \n");
         print("\t Hub up-link port p4 to Internet port number ", $host4, ":", $port4, "\n \n");
      } else {
         print("Mapping \n");
         print("\t No up-link defined \n \n");
      }

      Rube::Collection::SYSTEM->set_name("Peter_Hub");
      Rube::Collection::SYSTEM->set_type('HUB');
      Rube::Collection::FLAG::->set_trace(FALSE);

      print("*************************************************\n");
      print("*****               End Setup               *****\n");
      print("*************************************************\n \n");

      my $name;

      # ================ PORTS =================

      $name = "p0";
      $sem = "p0Sm";
      Rube::Table::IFACE->set_packetType($name, 'RUBEG');
      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Rube::Fsm::SocConS->new(
            taskName => $name,
            taskSem => $sem,
            ifaceName => $name,
            port => $port0
         )
      );
      Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
      #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
      Tosf::Table::TASK->reset($name);

      $name = "p1";
      $sem = "p1Sm";
      Rube::Table::IFACE->set_packetType($name, 'RUBEG');
      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Rube::Fsm::SocConS->new(
            taskName => $name,
            taskSem => $sem,
            ifaceName => $name,
            port => ($port0 + 1)
         )
      );
      Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
      #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
      Tosf::Table::TASK->reset($name);

      $name = "p2";
      $sem = "p2Sm";
      Rube::Table::IFACE->set_packetType($name, 'RUBEG');
      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Rube::Fsm::SocConS->new(
            taskName => $name,
            taskSem => $sem,
            ifaceName => $name,
            port => ($port0 + 2)
         )
      );
      Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
      #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
      Tosf::Table::TASK->reset($name);

      $name = "p3";
      $sem = "p3Sm";
      Rube::Table::IFACE->set_packetType($name, 'RUBEG');
      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         fsm => Rube::Fsm::SocConS->new(
            taskName => $name,
            taskSem => $sem,
            ifaceName => $name,
            port => ($port0 + 3)
         )
      );
      Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
      #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
      Tosf::Table::TASK->reset($name);

      if ($host4 ne 'none') {

         $name = "p4"; #(Up link)
         $sem = "p4Sm";
         Rube::Table::IFACE->set_packetType($name, 'RUBEG');
         Tosf::Table::TASK->new(
            name => $name, 
            periodic => TRUE, 
            period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
            fsm => Rube::Fsm::SocConC->new(
               taskName => $name,
               taskSem => $sem,
               ifaceName => $name,
               host => $host4,
               port => $port4
            )
         );
         Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
         #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
         Tosf::Table::TASK->reset($name);
      }

      $name = "Hub";
      $sem = "HubSm";
      Tosf::Table::TASK->new(
         name => $name,
         periodic => FALSE,
         fsm => Rube::Fsm::Hub->new(
            taskName => $name,
            taskSem => $sem,
         )
      );
      Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
      #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
      Tosf::Table::TASK->reset($name);

   } elsif ($sel == 2) {

      # no parameter checking is performed on this user data :(
      my $numPorts = 4;
      print("*************************************************\n");
      print("*****              Switch Setup             *****\n");
      print("*************************************************\n \n");
      
      print("How many ports do we require: ");
      chomp($numPorts = <>);
      
      print("$numPorts consecutive Tcp Internet port numbers are required \n");
      print("on localhost for Switch ports p0 through p".($numPorts-1)." \n");
      print("Enter Internet port number for Switch p0: ");
      chomp($port0 = <>);
      print("Mapping \n");
      my $i = 0;
      while ($i < $numPorts) {
         print("\t Switch port p".$i." to Internet port number ", $port0 + $i, "\n");
         $i++;
      }
      print("\n");

      print("One Tcp Internet port number is required on a connected \n");
      print("host for Switch up-link port p4 \n");
      print("Enter Internet host name for Switch p4 (enter none to disable p4): ");
      my $host = "localhost";
      my $port = 4444;
      chomp($host = <>);
      if ($host ne "none") {
         print("Enter Internet port number for Switch p4: ");
         chomp($port = <>);
         print("Mapping \n");
         print("\t Switch up-link port p4 to Internet port number ", $host, ":", $port, "\n \n");
      } else {
         print("Mapping \n");
         print("\t No up-link defined \n \n");
      }

      Rube::Collection::SYSTEM->set_name("Peter_Switch");
      Rube::Collection::SYSTEM->set_type('SWITCH');
      Rube::Collection::FLAG::->set_trace(FALSE);

      print("*************************************************\n");
      print("*****               End Setup               *****\n");
      print("*************************************************\n \n");

      my $name;

      # ================ PORTS =================
      $i = 0;
      while ($i < $numPorts) {
         $name = "p".$i;
         $sem = "p".$i."Sm";
         Rube::Table::IFACE->set_packetType($name, 'RUBEG');
         Tosf::Table::TASK->new(
            name => $name, 
            periodic => TRUE, 
            period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
            fsm => Rube::Fsm::SocConS->new(
               taskName => $name,
               taskSem => $sem,
               ifaceName => $name,
               port => $port0+$i
            )
         );
         Tosf::Table::SEMAPHORE->add(name => $sem, max => 1);
         #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
         Tosf::Table::TASK->reset($name);
         $i++;
      }

      if ($host ne 'none') {

         $name = "p".$i; #(Up link)
         $sem = "p".$i."Sm";
         Rube::Table::IFACE->set_packetType($name, 'RUBEG');
         Tosf::Table::TASK->new(
            name => $name, 
            periodic => TRUE, 
            period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
            fsm => Rube::Fsm::SocConC->new(
               taskName => $name,
               taskSem => $sem,
               ifaceName => $name,
               host => $host,
               port => $port
            )
         );
         Tosf::Table::SEMAPHORE->add(name => $sem, max => 1);
         #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
         Tosf::Table::TASK->reset($name);
      }

      $name = "Switch";
      $sem = "SwitchSm";
      Tosf::Table::TASK->new(
         name => $name,
         periodic => FALSE,
         fsm => Rube::Fsm::Switch->new(
            taskName => $name,
            taskSem => $sem,
         )
      );
      Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
      #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
      Tosf::Table::TASK->reset($name);
   
      $name = "SwitchTablePurge";
      $sem = "SwitchTablePurgeSm";
      Tosf::Table::TASK->new(
         name => $name,
         periodic => TRUE,
         period => Tosf::Executive::TIMER->s2t(1),
         fsm => Rube::Fsm::SwitchTablePurge->new(
            taskName => $name,
            taskSem => $sem,
         )
      );
      Tosf::Table::SEMAPHORE->add(name => $sem, max => 1);
      #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
      Tosf::Table::TASK->reset($name);

   }


   # All SYSTEM types require the following tasks.

   # ========== IFACE CONTROLLER  ============

      $name = "IfaceCon";
      $sem = "IfaceConSm";
      Tosf::Table::TASK->new(
         name => $name,
         periodic => FALSE,
         run => FALSE,
         fsm => Rube::Fsm::IfaceCon->new(
            taskName => $name,
            taskSem => $sem,
         )
      );
      Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
      #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
      Tosf::Table::TASK->reset($name);

   # ============== STREAM CONTROLLER  ==============
  
   $name = "s0";
   $sem = "s0Sm";
   Rube::Table::IFACE->set_packetType($name, 'STREAM');
   Tosf::Table::TASK->new(
      name => $name, 
      periodic => TRUE, 
      period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
      run => TRUE,
      fsm => Rube::Fsm::StreamCon->new(
         taskName => $name,
         taskSem => $sem,
         ifaceName => $name,
      )
   );
   Tosf::Table::SEMAPHORE->add(name => $sem, max => 1);
   #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
   Tosf::Table::TASK->reset($name);

   $name = "Shell";
   $sem = "ShellSm";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      run => FALSE,
      fsm => rubeApp::Fsm::Shell->new(
         taskName => $name,
         taskSem => $sem,
      )
   );
   Tosf::Table::SEMAPHORE->add(name => $sem, max => 10);
   #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
   Tosf::Table::TASK->reset($name);

   # ================ HEARTBEAT =================
  
   $name = "HBeat";
   $sem = "HBeatSm";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => TRUE,
      period => Tosf::Executive::TIMER->s2t(10),
      run => TRUE,
      fsm => Rube::Fsm::HBeat->new(
         taskName => $name,
         taskSem => $sem,
      )
   );
   Tosf::Table::SEMAPHORE->add(name => $sem, max => 1);
   #Tosf::Table::SEMAPHORE->wait(semaphore => $sem, task => $name);
   Tosf::Table::TASK->reset($name);

}

1;
