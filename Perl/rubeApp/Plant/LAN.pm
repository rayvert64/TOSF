package rubeApp::Plant::LAN;
#================================================================--
# File Name    : LAN.pm
#
# Purpose      : Plant set-up for LAN 
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;
use constant TRUE => 1;
use constant FALSE => 0;
use constant MAXSEL => 2 ;
use constant IFACEPOLLFREQ => 0.05;
use constant EARTHMAC => '0';
use constant WINDMAC => '0';
use constant FIREMAC => '0';

sub start {
   system('clear');

   my $inp;
   my $sel = -1;
   my $port;
   my $host;

   while (($sel < 0) || ($sel > MAXSEL))  {

      print ("\nLAN Boot Menu\n\n");
      print ("\tEarth 0\n");
      print ("\tWind 1\n");
      print ("\tFire 2\n");
      print ("\nEnter selection (CTRL C to exit) ");
      $inp = <>;
      chop($inp);
      if (($inp =~ m/\d/) && (length($inp) == 1)) {
         $sel = int($inp);
      }

   }

   my $name;

   if ($sel == 0) {

      # Earth 
      Rube::Collection::SYSTEM->set_name("Earth");
      Rube::Collection::SYSTEM->set_type('HOST');

      $host = 'localhost';
      $port = 5071;

      # no parameter checking is performed on this user data :(

      # ================ PORT =================

      $name = "eth0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         run => TRUE,
         fsm => Rube::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      # note, Ethernet, Ip , Icmp etc are set up in Rube::Plant
   
      Rube::Table::IFACE->set_mac($name, EARTHMAC);
      Rube::Table::IFACE->set_packetType($name, 'RUBEG');

      Rube::Table::ROUTE->set_route('0.0.0.0', '0.0.0.0', $name);

   } elsif ($sel == 1) {

      # Wind 
      Rube::Collection::SYSTEM->set_name("Wind");
      Rube::Collection::SYSTEM->set_type('HOST');

      $host = 'localhost';
      $port = 5072;

      # no parameter checking is performed on this user data :(

      # ================ PORT =================

      $name = "eth0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         run => TRUE,
         fsm => Rube::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      # note, Ethernet, IP , ICMP are set up in Rube::Plant
   
      Rube::Table::IFACE->set_mac($name, WINDMAC);
      Rube::Table::IFACE->set_packetType($name, 'RUBEG');

      Rube::Table::ROUTE->set_route('0.0.0.0', '0.0.0.0', $name);

   } elsif ($sel == 2) {
      
      # Fire
      Rube::Collection::SYSTEM->set_name("Fire");
      Rube::Collection::SYSTEM->set_type('HOST');

      $host = 'localhost';
      $port = 5073;

      # no parameter checking is performed on this user data :(

      # ================ PORT =================

      $name = "eth0";

      Tosf::Table::TASK->new(
         name => $name, 
         periodic => TRUE, 
         period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
         run => TRUE,
         fsm => Rube::Fsm::SocConC->new(
            taskName => $name,
            ifaceName => $name,
            host => $host,
            port => $port
         )
      );
      Tosf::Table::TASK->reset($name);

      # note, Ethernet, IP , ICMP are set up in Rube::Plant
   
      Rube::Table::IFACE->set_mac($name, FIREMAC);
      Rube::Table::IFACE->set_packetType($name, 'RUBEG');

      Rube::Table::ROUTE->set_route('0.0.0.0', '0.0.0.0', $name);

   }   

   # All SYSTEM types require the following tasks.

   # ========== IFACE CONTROLLER  ============

   $name = "IfaceCon";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      run => FALSE,
      fsm => Rube::Fsm::IfaceCon->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   # ============== STREAM CONTROLLER  ==============

   $name = "Timer";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      run => FALSE,
      fsm => rubeApp::Fsm::Timer->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   $name = "s0";
   Rube::Table::IFACE->set_packetType($name, 'STREAM');
   Tosf::Table::TASK->new(
      name => $name,
      periodic => TRUE,
      period => Tosf::Executive::TIMER->s2t(IFACEPOLLFREQ),
      run => TRUE,
      fsm => Rube::Fsm::StreamCon->new(
         taskName => $name,
         ifaceName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   $name = "Shell";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      run => FALSE,
      fsm => rubeApp::Fsm::Shell->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

   $name = "Rubed";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => FALSE,
      run => FALSE,
      fsm => rubeApp::Fsm::Rubed->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);


   # ================ HEARTBEAT =================

   $name = "HBeat";
   Tosf::Table::TASK->new(
      name => $name,
      periodic => TRUE,
      period => Tosf::Executive::TIMER->s2t(20),
      run => TRUE,
      fsm => Rube::Fsm::HBeat->new(
         taskName => $name,
      )
   );
   Tosf::Table::TASK->reset($name);

}

1;
