#!/usr/bin/perl
#========================================================
# Project      : Time Oriented Software Framework
#
# File Name    : main.pl
#
# Purpose      : main rubeApp routine for inet applications
#
# Author       : Peter Walsh, Vancouver Island University
#
# System       : Perl (Linux)
#
#========================================================

$SIG{INT} = sub { leaveScript(); };

$SIG{PIPE} = 'IGNORE';

$SIG{ALRM} = sub {
    my $tl = Tosf::Exception::Monitor->new(
        fn => sub {
            Tosf::Executive::SCHEDULER->tick();
        }
    );
    $tl->run();
};

$| = 1;

use strict;
use warnings;
no warnings "experimental::smartmatch";

#use Sys::Mlockall qw(:all);
#if (mlockall(MCL_CURRENT | MCL_FUTURE) != 0) { die "Failed to lock RAM: $!"; }

use lib '../';
use FileHandle;
use Try::Tiny;
use Time::HiRes qw (ualarm usleep);
use Scalar::Util qw(looks_like_number);

use IO::Poll qw(POLLPRI POLLERR);                                    
use IO::Socket::INET;
use IO::Select;
use Tosf::Table::QUEUE;
use Tosf::Table::PQUEUE;
use Tosf::Table::TASK;
use Tosf::Table::SEMAPHORE;
use Tosf::Record::SVar;
use Tosf::Record::Task;
use Tosf::Record::Semaphore;
use Tosf::Record::Message;
use Tosf::Collection::Queue;
use Tosf::Collection::PQueue;
use Tosf::Collection::Line;
use Tosf::Collection::STATUS;
use Tosf::Exception::Trap;
use Tosf::Exception::Monitor;
use Tosf::Table::SVAR;
use Tosf::Table::TASK;
use Tosf::Table::SEMAPHORE;
use Tosf::Table::MESSAGE;
use Tosf::Executive::TIMER;
use Tosf::Executive::SCHEDULER;
use Tosf::Executive::DISPATCHER;
use Tosf::Fsm::To;

#use Rube::Packet::Ip;
#use Rube::Packet::Arp;
#use Rube::Packet::Icmp;
use Rube::Packet::RubeG;
use Rube::Packet::Generic;
use Rube::Collection::FLAG;
use Rube::Collection::SYSTEM;
use Rube::Collection::SysfsGPIO;
use Rube::Record::Iface;
use Rube::Record::Rubeg;
use Rube::Record::Switch;
use Rube::Record::Route;
use Rube::Record::SocketUdp;
use Rube::Record::SystemMsg;
use Rube::Plant::SETUP;
use Rube::Table::IFACE;
use Rube::Table::RUBEG;
use Rube::Table::SWITCH;
use Rube::Table::ROUTE;
use Rube::Table::SOCKETUDP;
use Rube::Table::SYSTEMMSG;
use Rube::Fsm::IfaceCon;
use Rube::Fsm::SocConS;
use Rube::Fsm::SocConC;
use Rube::Fsm::StreamCon;
use Rube::Fsm::PinChkCon;

#use Rube::Fsm::Arp;
#use Rube::Fsm::Ip;
#use Rube::Fsm::Icmp;
use Rube::Fsm::Hub;
use Rube::Fsm::Sniffer;
use Rube::Fsm::Switch;
use Rube::Fsm::SwitchTablePurge;
use Rube::Fsm::HBeat;
use Rube::Fsm::RubeG;

use rubeApp::Fsm::Shell;
#use rubeApp::Fsm::Inc;
#use rubeApp::Fsm::Incd;
use rubeApp::Fsm::Timer;
use rubeApp::Fsm::Rube;
use rubeApp::Plant::SETUP;
use rubeApp::Plant::LAN;
use rubeApp::Plant::MAN;
use rubeApp::Plant::WAN;
use rubeApp::Plant::HOST;
use rubeApp::Plant::NODE;
use rubeApp::Plant::PYB;

use constant TRUE  => 1;
use constant FALSE => 0;

#timer period in seconds
use constant TIMER_PERIOD => 0.005;
use constant NUM_SLOTS             => 3;
use constant SCHEDULING_DISCIPLINE => "EDF";
use constant REAL_TIME_TYPE        => "SOFT";

sub leaveScript {
    my $ev = shift @_;

    print("\nShutdown Now !!!!! \n");
    my $fsm = Tosf::Table::TASK->get_fsm("Rube");
    if(defined($fsm)) {
        $fsm->destroy();
    }

    if ( defined($ev) ) {    
        exit($ev);
    }
    else {
        exit(0);
    }
}

my $tl = Tosf::Exception::Monitor->new(
    fn => sub {

        Tosf::Executive::DISPATCHER->set_num_slots(NUM_SLOTS);
        Tosf::Executive::TIMER->set_period(TIMER_PERIOD);

        Rube::Collection::FLAG->set_trace(TRUE);

        Rube::Plant::SETUP->start();
        rubeApp::Plant::SETUP->start();

        #Tosf::Table::TASK->reset_all();

        Tosf::Executive::TIMER->start();

        while (1) {
            if ( !Tosf::Collection::STATUS->get_cycleComplete() ) {
                Tosf::Executive::DISPATCHER->tick();
                wait();
            }
        }

    }
);

$tl->run();
