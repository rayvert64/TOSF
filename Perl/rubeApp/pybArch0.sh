#!/bin/sh
#========================================================
# Project      : Time Oriented Software Framework
#
# File Name    : pybArch0.sh
#
# Purpose      : Deploy PYB Architecture 0
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Dash Shell Script (Linux)
#
#========================================================

printf "1\n3\n0\nlocalhost\n42070\n1\nn\n" > .k2 
xterm -fa 'Monospace' -fs 10 -geometry 118x29+960+550 -title "PYB0" -hold -e "./main.pl .k2" &
printf "1\n3\n0\nlocalhost\n42071\n2\nn\n" > .k3 
xterm -fa 'Monospace' -fs 10 -geometry 118x29+1920+0 -title "PYB1" -hold -e "./main.pl .k3" &
printf "1\n3\n0\nlocalhost\n42072\n3\nn\n" > .k4 
xterm -fa 'Monospace' -fs 10 -geometry 118x29+1920+550 -title "PYB2" -hold -e "./main.pl .k4" &
printf "1\n3\n0\nlocalhost\n42073\n4\nn\n" > .k5 
xterm -fa 'Monospace' -fs 10 -geometry 118x29+2880+0 -title "PYB3" -hold -e "./main.pl .k5" &

#printf "1\n3\n0\nlocalhost\n42082\n13\nn\n" > .k14 
#xterm -fa 'Monospace' -fs 10 -geometry 118x29+2880+0 -title "PYB12" -hold -e "./main.pl .k14" &
#printf "1\n3\n0\nlocalhost\n42083\n14\nn\n" > .k15
#xterm -fa 'Monospace' -fs 10 -geometry 118x29+2880+0 -title "PYB13" -hold -e "./main.pl .k15" &
#printf "1\n3\n0\nlocalhost\n42084\n15\nn\n" > .k16
#xterm -fa 'Monospace' -fs 10 -geometry 118x29+2880+0 -title "PYB14" -hold -e "./main.pl .k16" &
